# encoding=utf-8
"""
05/08/22
    don't know if this is being used for anything currently
    disabled from RunAll until i can get clarification
06/09/22
    appears that this is required to update the sales numbers which are the basis of the pay p;lan
"""
import luigi
import utilities
import datetime
import fact_gl

pipeline = 'service_advisor_payroll'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


class UpdateSalesDetail(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_gl.FactGl()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select sap.update_sales_details();')


# if __name__ == '__main__':
#     luigi.run(["--workers=4"], main_task_cls=UpdateSalesDetail)
