# encoding=utf-8
"""
when this fucking things fails for a missing disclaimer, the fix it script is:
  .../misc_sql/incentives/dealeron_feed.sql
    need to manually run the individual parts of select ifd.create_dealeron_feed()
    then can see what is failing the assert

the actual diclaimer information is to be found on rydellautocenter.com

12/22/20 moved to luigi, won't run from home because of ip access to ftp site, need to run it from server
    this is run daily after the global connect incentives has run and any required incentive admin is complete
    run manually, not scheduled because of the incentive admin dependency which is unpredictable
"""

import luigi
import utilities
import csv
import ftplib
import datetime

pipeline = 'dealeron'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
extract_file = 'dealeron/rydell.csv'
file_name = 'rydell.csv'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class RebateFeed(luigi.Task):

    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select ifd.create_dealeron_feed()")
                pg_cur.execute("select * from ifd.dealeron_feed")
                with open(extract_file, 'w') as f:
                    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
                    writer.writerow(["rebates", "vin", "dealerid"])
                    writer.writerows(pg_cur)
        server = 'ftp.dealeron.com'
        username = 'rydell'
        password = '9B!mT2DgQA'
        ftp = ftplib.FTP(server)
        ftp.login(username, password)
        ftp.storbinary('STOR ' + file_name, open(extract_file, 'rb'))
        ftp.quit()


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=RebateFeed)
