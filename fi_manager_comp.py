# encoding=utf-8
"""
  runs only on the first of the month
  updates the table sls.fi_manager_deals & sls.fi_manager_comp
  todo add the emailing to employees
"""

import luigi
import utilities
import datetime
import sc_payroll_201803

pipeline = 'sales_consultant_payroll'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


# ---------------------------------------------------------------------------------------- #
# if these task are being called via run_all, that task will handle all of this logging
# uncomment if running outside of run_all
# ---------------------------------------------------------------------------------------- #
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class UpdateFiManagerComp(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield sc_payroll_201803.ConsultantPayroll()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select employee_number, percentage
                    from sls.fi_managers
                    where active 
                      and current_row;                
                """
                pg_cur.execute(sql)
                for row in pg_cur.fetchall():
                    emp = str(row[0])
                    perc = row[1]
                    with pg_con.cursor() as pg_cur2:
                        sql = """
                            select sls.update_fi_manager_comp('{}',{})
                        """.format(emp, perc)
                        pg_cur2.execute(sql)


# if __name__ == '__main__':
#     luigi.run(["--workers=4"], main_task_cls=UpdateFiManagerComp)
