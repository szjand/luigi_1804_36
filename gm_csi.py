# encoding=utf-8
"""
01/29/21: one of the things i need to accomplish with this stuff is the ability to retry it
    several times.  these pages being scraped are overly complex and somewhat touchy
    so, that's been on my list
    but today, trying to get SalesMake to sork, rerunning it manually a bunchc of times, got this
    error email:
        luigi-client@ubuntu-1804
        Luigi Scheduler: DISABLED SalesMakeCSIParse__99914b932b due to excessive failures
        SalesMakeCSIParse__99914b932b failed 3 times in the last 3600 seconds,
            so it is being disabled for 86400 seconds
    hmmmm

cd /home/jon/projects/luigi_1804_36 && /home/jon/Desktop/venv/luigi/bin/python3 -m luigi --module gm_csi SalesMakeCSIParse
cd /home/jon/projects/luigi_1804_36 && /home/jon/Desktop/venv/luigi/bin/python3 -m luigi --module gm_csi ServiceMakeCSIParse
cd /home/jon/projects/luigi_1804_36 && /home/jon/Desktop/venv/luigi/bin/python3 -m luigi --module gm_csi ServiceAdvisorCSIParse

02/21/21
when these fail it is usually at:
  File "/home/jon/projects/luigi_1804_36/gm_csi.py", line 532 (362, in get_files
    (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
need to toughen up that code somehow

03/09/21
    reputation.com link xpath changed
"""

import utilities
import luigi
import datetime
from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import requests
import os
import csv


pipeline = 'gm_csi'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())
user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None

# local_or_production = 'production'
local_or_production = 'local'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


# noinspection PyMethodMayBeStatic
class ServiceAdvisorCSIDownload(luigi.Task):
    """
    """
    pipeline = pipeline

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/service_advisor_csi/download/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/service_csi/download/'

    def local_target(self):
        return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.download_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def login(self, driver):
        # get username and password fields
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        username_box = driver.find_element_by_name('IDToken1')
        password_box = driver.find_element_by_name('IDToken2')
        login_btn = driver.find_element_by_class_name('login')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")

    def get_files(self, driver):
        def download_csv():
            # click elipsis
            # don't know if this will help, but intermittent nature of failure has me puzzled
            time.sleep(5)
            WebDriverWait(driver, 45).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[2]'
                           '/div[2]/div/div/div/div[2]/div[2]/div[2]/div/div/div[2]/a'))).click()
            # click Download CSV
            WebDriverWait(driver, 45).until(ec.element_to_be_clickable(
                    (By.XPATH, '/html/body/div[2]/div/div/div/section/article[2]'
                               '/div[2]/div/div/div/div[2]/div[2]/div[2]/div/div/div[2]/ul/li[2]/a'))).click()
        # click shortcut to Reputation.com
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="nav"]/li[2]/ul/li[2]/a'))).click()
        WebDriverWait(driver, 20).until(ec.number_of_windows_to_be(2))
        driver.switch_to.window(driver.window_handles[1])
        # click right array to open menu
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[1]/div/i'))).click()
        # click Created By Me
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[1]/h4/a'))).click()

        # BUICK ########################################################
        # click BuickMTD
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[4]/a'))).click()
        download_csv()
        time.sleep(20)
        # click BuickQTD
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[5]/a'))).click()
        download_csv()
        time.sleep(20)
        # click BuickYTD
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[6]/a'))).click()
        download_csv()
        time.sleep(20)
        # CHEVY ########################################################
        # click ChevyMTD
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[7]/a'))).click()
        download_csv()
        time.sleep(20)
        # click ChevyQTD
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[8]/a'))).click()
        download_csv()
        time.sleep(20)
        # click ChevyYTD
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[9]/a'))).click()
        download_csv()
        time.sleep(20)
        # GMC ########################################################
        # click GMCMTD
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[10]/a'))).click()
        download_csv()
        time.sleep(20)
        # click GMCQTD
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[11]/a'))).click()
        download_csv()
        time.sleep(20)
        # click GMCYTD
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[12]/a'))).click()
        download_csv()
        time.sleep(20)

        driver.close()
        driver.switch_to.window(driver.window_handles[0])

    def run(self):
        driver = self.setup_driver()
        try:
            for filename in os.listdir(self.download_dir):
                os.unlink(self.download_dir + filename)
            driver.get('https://www.gmglobalconnect.com')
            self.login(self, driver)
            self.get_files(driver)
        finally:
            driver.quit()
        assert len(os.listdir(self.download_dir)) == 9


class ServiceAdvisorCSIParse(luigi.Task):
    """
    """
    pipeline = pipeline

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/service_advisor_csi/download/'
        final_csv_dir = '/home/jon/projects/luigi_1804_36/global_connect/service_advisor_csi/final_csv/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/service_csi/download/'
        final_csv_dir = '/home/rydell/projects/luigi_1804_36/global_connect/service_csi/final_csv/'

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        # return None
        yield ServiceAdvisorCSIDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        for filename in os.listdir(self.final_csv_dir):
            os.unlink(self.final_csv_dir + filename)
        for file_name in os.listdir(self.download_dir):
            # adds the filename to each row in the csv
            with open(self.download_dir + file_name, 'r') as read_obj, open(
                    self.final_csv_dir + file_name, 'w', newline='') as write_obj:
                reader = csv.reader(read_obj)
                writer = csv.writer(write_obj)
                for row in reader:
                    row.append(file_name)
                    writer.writerow(row)
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(self.final_csv_dir + file_name, 'r') as io:
                        pg_cur.copy_expert("copy gmgl.service_advisor_csi_ext from stdin with CSV HEADER "
                                           "encoding 'latin-1 '", io)
        assert len(os.listdir(self.final_csv_dir)) == 9
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select gmgl.service_advisor_csi_update();"
                pg_cur.execute(sql)
                sql = "truncate gmgl.service_advisor_csi_ext"
                pg_cur.execute(sql)


class ServiceMakeCSIDownload(luigi.Task):
    """
    """
    pipeline = pipeline
    retry_count = 3

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36//global_connect/service_make_csi/download/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/service_make_csi/download/'

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.download_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def login(self, driver):
        # get username and password fields
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        username_box = driver.find_element_by_name('IDToken1')
        password_box = driver.find_element_by_name('IDToken2')
        login_btn = driver.find_element_by_class_name('login')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")

    def get_files(self, driver):
        def download_csv():
            # click elipsis
            WebDriverWait(driver, 30).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[2]'
                           '/div[2]/div/div/div/div[2]/div[2]/div[2]/div/div/div[2]/a'))).click()
            # click Download CSV
            WebDriverWait(driver, 30).until(ec.element_to_be_clickable(
                    (By.XPATH, '/html/body/div[2]/div/div/div/section/article[2]'
                               '/div[2]/div/div/div/div[2]/div[2]/div[2]/div/div/div[2]/ul/li[2]/a'))).click()
        # click shortcut to Reputation.com
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="nav"]/li[2]/ul/li[2]/a'))).click()
        WebDriverWait(driver, 20).until(ec.number_of_windows_to_be(2))
        driver.switch_to.window(driver.window_handles[1])
        # click right array to open menu
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[1]/div/i'))).click()
        # click Created By Me
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[1]/h4/a'))).click()
        # created_by_me = WebDriverWait(driver, 30).until(ec.element_to_be_clickable(
        #     (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]/div[2]/div[1]'
        #                '/div[1]/h4/a')))
        # driver.execute_script("arguments[0].click();", created_by_me)

        # BUICK ########################################################
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[13]/a'))).click()
        download_csv()
        time.sleep(20)

        # CHEVY ########################################################
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[14]/a'))).click()

        download_csv()
        time.sleep(20)

        # GMC ########################################################
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[15]/a'))).click()
        download_csv()
        time.sleep(20)

        driver.close()
        driver.switch_to.window(driver.window_handles[0])

    def run(self):
        driver = self.setup_driver()
        try:
            for filename in os.listdir(self.download_dir):
                os.unlink(self.download_dir + filename)
            driver.get('https://www.gmglobalconnect.com')
            self.login(self, driver)
            self.get_files(driver)
        finally:
            driver.quit()
        assert len(os.listdir(self.download_dir)) == 3


class ServiceMakeCSIParse(luigi.Task):
    """
    """
    pipeline = pipeline
    retry_count = 3

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/service_make_csi/download/'
        final_csv_dir = '/home/jon/projects/luigi_1804_36/global_connect/service_make_csi/final_csv/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/service_make_csi/download/'
        final_csv_dir = '/home/rydell/projects/luigi_1804_36/global_connect/service_make_csi/final_csv/'

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        yield ServiceMakeCSIDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        for filename in os.listdir(self.final_csv_dir):
            os.unlink(self.final_csv_dir + filename)
        for file_name in os.listdir(self.download_dir):
            # adds the filename to each row in the csv
            with open(self.download_dir + file_name, 'r') as read_obj, open(
                    self.final_csv_dir + file_name, 'w', newline='') as write_obj:
                reader = csv.reader(read_obj)
                writer = csv.writer(write_obj)
                for row in reader:
                    row.append(file_name)
                    writer.writerow(row)
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(self.final_csv_dir + file_name, 'r') as io:
                        pg_cur.copy_expert("copy gmgl.service_make_csi_ext from stdin with CSV HEADER "
                                           "encoding 'latin-1 '", io)
        assert len(os.listdir(self.final_csv_dir)) == 3
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select gmgl.service_make_csi_update();"
                pg_cur.execute(sql)
                sql = "truncate gmgl.service_make_csi_ext"
                pg_cur.execute(sql)


class SalesMakeCSIDownload(luigi.Task):
    """
    """
    pipeline = pipeline
    retry_count = 3

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36//global_connect/sales_make_csi/download/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/sales_make_csi/download/'

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.download_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def login(self, driver):
        # get username and password fields
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        username_box = driver.find_element_by_name('IDToken1')
        password_box = driver.find_element_by_name('IDToken2')
        login_btn = driver.find_element_by_class_name('login')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")

    def get_files(self, driver):
        def download_csv():
            # click elipsis
            WebDriverWait(driver, 30).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[2]'
                           '/div[2]/div/div/div/div[2]/div[2]/div[2]/div/div/div[2]/a'))).click()
            # click Download CSV
            WebDriverWait(driver, 30).until(ec.element_to_be_clickable(
                    (By.XPATH, '/html/body/div[2]/div/div/div/section/article[2]'
                               '/div[2]/div/div/div/div[2]/div[2]/div[2]/div/div/div[2]/ul/li[2]/a'))).click()
        # click shortcut to Reputation.com
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="nav"]/li[2]/ul/li[2]/a'))).click()
        WebDriverWait(driver, 20).until(ec.number_of_windows_to_be(2))
        driver.switch_to.window(driver.window_handles[1])
        # click right array to open menu
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[1]/div/i'))).click()
        # click Created By Me
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[1]/h4/a'))).click()

        # BUICK ########################################################
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[1]/a'))).click()
        download_csv()
        time.sleep(20)
        for filename in (os.listdir(self.download_dir)):
            with open(self.download_dir + filename, 'r') as f:
                reader = csv.reader(f)
                data = list(reader)
                if len(data) != 7:
                    raise Exception('Buick file is missing one or more rows')


        # CHEVY ########################################################
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[2]/a'))).click()

        download_csv()
        time.sleep(20)

        # GMC ########################################################
        WebDriverWait(driver, 20).until(ec.element_to_be_clickable(
                (By.XPATH, '/html/body/div[2]/div/div/div/section/article[1]/div/div/div[2]'
                           '/div[2]/div[1]/div[2]/div/ul/li[3]/a'))).click()
        download_csv()
        time.sleep(20)

        driver.close()
        driver.switch_to.window(driver.window_handles[0])

    def run(self):
        driver = self.setup_driver()
        try:
            for filename in os.listdir(self.download_dir):
                os.unlink(self.download_dir + filename)
            driver.get('https://www.gmglobalconnect.com')
            self.login(self, driver)
            self.get_files(driver)
        finally:
            driver.quit()
        assert len(os.listdir(self.download_dir)) == 3


class SalesMakeCSIParse(luigi.Task):
    """
    """
    pipeline = pipeline
    retry_count = 3

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/sales_make_csi/download/'
        final_csv_dir = '/home/jon/projects/luigi_1804_36/global_connect/sales_make_csi/final_csv/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/sales_make_csi/download/'
        final_csv_dir = '/home/rydell/projects/luigi_1804_36/global_connect/sales_make_csi/final_csv/'

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        yield SalesMakeCSIDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        for filename in os.listdir(self.final_csv_dir):
            os.unlink(self.final_csv_dir + filename)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate gmgl.sales_make_csi_ext')
        for file_name in os.listdir(self.download_dir):
            # adds the filename to each row in the csv
            with open(self.download_dir + file_name, 'r') as read_obj, open(
                    self.final_csv_dir + file_name, 'w', newline='') as write_obj:
                reader = csv.reader(read_obj)
                writer = csv.writer(write_obj)
                for row in reader:
                    row.append(file_name)
                    writer.writerow(row)
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(self.final_csv_dir + file_name, 'r') as io:
                        pg_cur.copy_expert("copy gmgl.sales_make_csi_ext from stdin with CSV HEADER "
                                           "encoding 'latin-1 '", io)
        assert len(os.listdir(self.final_csv_dir)) == 3
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select gmgl.sales_make_csi_update();')
                pg_cur.execute('truncate gmgl.sales_make_csi_ext;')


class CSI(luigi.WrapperTask):
    """
    invoked in a separate cron job: 9:10 AM monday thru friday
    """
    pipeline = pipeline

    def requires(self):
        yield ServiceAdvisorCSIParse()
        yield ServiceMakeCSIParse()


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=ServiceAdvisorCSIParse)
    # SalesMakeCSIParse ServiceMakeCSIParse ServiceAdvisorCSIParse SalesMakeCSIDownload
