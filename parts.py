# encoding=utf-8
"""

"""
from openpyxl import load_workbook
import utilities
import luigi
import datetime
import csv
import shutil
import requests

pipeline = 'parts'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())


class RoSalesLastYearNextMonth(luigi.Task):
    """
    report for fred, run on the 15th of each month, what parts were sold on ros last year next month
    """
    extract_file = '../../projects/luigi_1804_36/parts/next_month_last_year.csv'
    template = '../../projects/luigi_1804_36/parts/last_year_next_month_template.xlsx'
    new_spreadsheet = '../../projects/luigi_1804_36/parts/last_year_next_month.xlsx'
    pipeline = pipeline

    def local_target(self):
        return '../../luigi_output_files/' + ts_no_spaces + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select * from  pts.report_next_month_last_year();"
                pg_cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(pg_cur)
        shutil.copy(self.template, self.new_spreadsheet)
        wb = load_workbook(filename=self.new_spreadsheet)
        page = wb.active
        with open(self.extract_file) as f:
            reader = csv.reader(f, delimiter=',')
            for row in reader:
                page.append(row)
        wb.save(self.new_spreadsheet)
        dt = datetime.datetime.today()
        requests.post(
            "https://api.mailgun.net/v3/notify.cartiva.com/messages",
            auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
            files=[('attachment', ('last_year_next_month.xlsx', open(self.new_spreadsheet, 'rb').read()))],
            data={"from": "jandrews@cartiva.com <jon@notify.cartiva.com>",
                  "to": ['jandrews@cartiva.com', 'fvanheste@rydellcars.com'],
                  "subject": '{0} {1} Ro sales for next month last year'.format(str(dt.strftime('%B')),
                                                                                str(dt.strftime('%Y'))),
                  "text": "Good Morning Fred"})


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=RoSalesLastYearNextMonth)
