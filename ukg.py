# encoding=utf-8
"""
12/17/21
    added the_response.raise_for_status() to each request after i got burned on a bad token error
    oops, that did not catch
    {"errors": [{"code": 401, "message": "The access token invalid"}], "user_messages":
               [{"text": "The access token invalid", "details": {"code": 401}, "severity": "ERROR"}]}
    contrary to the requests docs : "If we made a bad request (a 4XX client error or 5XX server error response),
                                        we can raise it with Response.raise_for_status()"
    so do it in the functions that truncate the table, verify that there are records created

    so based on the work in today_arkona_ukg_clock_hours.py, added a test for errors in the json response in the
    function that processes the json into tables, eg, function ukg.ext_time_entries(), ukg.ext_timeoffs(), etc

12/22/21
    afton changed the token frequency from 60 minutes to 55 minutes, that seems to have largely eliminated
    this error

12/25/21
    ukg.json_cost_center_job_details is littered with lots of
    {"errors": [{"code": 100, "message": "Too many calls."}], "user_messages": [{"text": "Too many calls.", "details":
            {"code": 100}, "severity": "ERROR"}]}
    injected a 2 sec sleep, it passed,a but not much confidence, this api has been notoriously
    erratic, don't know kf it's me or them
    GetEmployeeDetails() uses the same pattern, read the ids into an array

    same thing with ukg.json_employee_pay_info, but it failed quietly because the table,
    ukg.ext_employee_pay_info does not have the not null constraints
    153 of 602 rows failed
    change the function to extract the id rather than using the array generated id, make the
    id not null
    the 2 sec sleep did not fix this one
12/25/21 drop the employee_pay_info, data not actually required, api is a pain in the ass with too many calls

12/31/2021
    implemented the population of ukg.clock_hours
01/24/22
    deployed the payroll stuff
01/30/22
    GetEmployeePayInfo & GetEmployeeProfiles limit the scrape to active employees
04/24/22
    After adding the 1 sec wait to PayStatementDetails, it is now taking 1 1/2 hours to do just that task (with 4284
    pay statements).  Limit the number of payroll ids to process based on the date
    tested locally, knocks the processing of PayStatementDetailsn down to 11 minutes, i'll take it
04/26/22
    added the 1 sec wait to GetEmployeePayInfo

todo
    sls.update_consultant_payroll() does not expose pto/hol rates unless there are hours
    fi_manager_comp
    ukg.update_commission_pay_groups: check for work arounds in the function to clean up
    sls.paid_by_month: can probably be converted ok to ukg, but i don't know what all the relevant earnings codes are
        for now, just do draw in sls.update_consultant_payroll() & sls.update_fi_manager_comp()
finished
    body shop previous month earnings no longer returns $$

12/7/22
    restored requires and call to 2 functions in class EmployeeProfiles.  don't know why they had been commented out.
     came up because the classification of honda flat rate techs in production summary was wrong because the table
      ukg.employee_profiles had not been updating since 11/1/2

"""
import luigi
import utilities
import datetime
import requests
import time

pipeline = 'ukg'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())


# ---------------------------------------------------------------------------------------- #
# if these task are being called via run_all, that task will handle all of this logging
# uncomment if running locally
# ---------------------------------------------------------------------------------------- #
# @luigi.Task.event_handler(luigi.Event.START)
# def task_start(self):
#     utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
#
#
# @luigi.Task.event_handler(luigi.Event.SUCCESS)
# def task_success(self):
#     utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
#     try:  # WrapperTest throws an AttributeError, see /Notes/issues
#         with self.output().open("w") as out_file:
#             # write to the output file
#             out_file.write('pass')
#     except AttributeError:
#         pass


# ---------------------------------------------------------------------
#   LOOK UPS
# ---------------------------------------------------------------------
class GetPayTypes(luigi.Task):
    """
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = 'https://beta.rydellvision.com:8888/ukg/lookup/pay-types'
        the_response = requests.request('GET', url)
        data = the_response.text
        data = data.replace("'", "''")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_pay_types')
                sql = """
                    insert into ukg.json_pay_types (response) values ('{0}');
                """.format(data)
                pg_cur.execute(sql)
                pg_con.commit()
                pg_cur.execute('select ukg.ext_pay_types()')


class GetEmployeeTypes(luigi.Task):
    """
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = 'https://beta.rydellvision.com:8888/ukg/lookup/employee-types'
        the_response = requests.request('GET', url)
        data = the_response.text
        data = data.replace("'", "''")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_employee_types')
                sql = """
                    insert into ukg.json_employee_types (response) values ('{0}');
                """.format(data)
                pg_cur.execute(sql)
                pg_con.commit()
                pg_cur.execute('select ukg.ext_employee_types()')


class GetPayCalcProfiles(luigi.Task):
    """
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = 'https://beta.rydellvision.com:8888/ukg/lookup/pay-cal-profiles'
        the_response = requests.request('GET', url)
        data = the_response.text
        data = data.replace("'", "''")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_pay_calc_profiles')
                sql = """
                    insert into ukg.json_pay_calc_profiles (response) values ('{0}');
                """.format(data)
                pg_cur.execute(sql)
                pg_cur.execute('select ukg.ext_pay_calc_profiles()')


class GetEarningsCodes(luigi.Task):
    """
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = 'https://beta.rydellvision.com:8888/ukg/lookup/earning-codes'
        the_response = requests.request('GET', url)
        data = the_response.text
        data = data.replace("'", "''")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_earnings_codes')
                sql = """
                    insert into ukg.json_earnings_codes (response) values ('{0}');
                """.format(data)
                pg_cur.execute(sql)
                pg_cur.execute('select ukg.ext_earnings_codes()')


class GetJobs(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = 'https://beta.rydellvision.com:8888/ukg/lookup/jobs'
        the_response = requests.request('GET', url)
        data = the_response.text
        data = data.replace("'", "''")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_jobs')
                sql = """
                    insert into ukg.json_jobs (response) values ('{0}');
                """.format(data)
                pg_cur.execute(sql)
                pg_cur.execute('select ukg.ext_jobs()')


# ---------------------------------------------------------------------
# TIME CLOCK
# ---------------------------------------------------------------------
class GetTimeoffs(luigi.Task):
    """
    initially using ts_with_minute for local_target
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = 'https://beta.rydellvision.com:8888/ukg/timeoffs'
        the_response = requests.request('GET', url)
        data = the_response.text
        data = data.replace("'", "''")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_timeoffs')
                sql = """
                    insert into ukg.json_timeoffs (response) values ('{0}');
                """.format(data)
                pg_cur.execute(sql)
                pg_cur.execute('select ukg.ext_timeoffs()')


class GetTimeEntries(luigi.Task):
    """
    initially using ts_with_minute for local_target
    """
    pipeline = pipeline
    # 31 days
    thru_date = datetime.date.today() - datetime.timedelta(days=1)
    from_date = thru_date - datetime.timedelta(days=30)

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetTimeoffs()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = ('https://beta.rydellvision.com:8888/ukg/time-entries?start_date='
               + str(self.from_date) + '&end_date=' + str(self.thru_date))
        the_response = requests.request('GET', url)
        data = the_response.text
        data = data.replace("'", "''")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_time_entries')
                sql = """
                    insert into ukg.json_time_entries (response) values ('{0}');
                """.format(data)
                pg_cur.execute(sql)
                pg_con.commit()
                pg_cur.execute('select ukg.ext_time_entries()')


class XfmClockHours(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetTimeEntries()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select ukg.xfm_clock_hours();
                """
                pg_cur.execute(sql)


# ---------------------------------------------------------------------
# EMPLOYEES
# ---------------------------------------------------------------------
class GetAllEmployees(luigi.Task):
    """
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = 'https://beta.rydellvision.com:8888/ukg/employees'
        the_response = requests.request('GET', url)
        data = the_response.text
        data = data.replace("'", "''")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_employees')
                sql = """
                    insert into ukg.json_employees (response) values ('{0}');
                """.format(data)
                pg_cur.execute(sql)
                pg_con.commit()
                pg_cur.execute('select ukg.ext_employees()')


class GetEmployeeDetails(luigi.Task):
    """
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetAllEmployees()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                id_array = []
                pg_cur.execute('truncate ukg.json_employee_details')
                sql = """
                    select employee_id
                    from ukg.ext_employees;
                """
                pg_cur.execute(sql)
                for row in pg_cur.fetchall():
                    id_array.append(str(row[0]))
                for the_id in id_array:
                    url = 'https://beta.rydellvision.com:8888/ukg/employees/' + the_id
                    the_response = requests.request('GET', url)
                    data = the_response.text
                    data = data.replace("'", "''")
                    time.sleep(1)
                    with pg_con.cursor() as pg_cur_2:
                        sql = """
                            insert into ukg.json_employee_details (id,response) values ({0},'{1}');
                        """.format(the_id, data)
                        pg_cur_2.execute(sql)
                        pg_con.commit()
                pg_cur.execute('select ukg.ext_employee_details()')


class UpdateEmployees(luigi.Task):
    """
    truncates and repopulates ukg.employees nightly
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetEmployeeDetails()
        yield GetCostCenters()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select ukg.update_employees();')


class GetEmployeePayInfo(luigi.Task):
    """
    01/30/2022: scraping data for active employees only.  this table has been flakey, sometimes
        it gets populated, sometimes not. restricting to active employees permits some not null
        constraints that will surface errors in the data
    04/26/22
        added the 1 sec wait to the iteration
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetEmployeeTypes()
        yield GetPayTypes()
        yield GetPersonalRateTables()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                id_array = []
                pg_cur.execute('truncate ukg.json_employee_pay_info')
                sql = """
                    select employee_id 
                    from ukg.ext_employees;
                """
                pg_cur.execute(sql)
                for row in pg_cur.fetchall():
                    id_array.append(str(row[0]))
                for the_id in id_array:
                    url = 'https://beta.rydellvision.com:8888/ukg/employees/' + the_id + '/pay-info'
                    the_response = requests.request('GET', url)
                    data = the_response.text
                    data = data.replace("'", "''")
                    time.sleep(1)
                    with pg_con.cursor() as pg_cur_2:
                        sql = """
                             insert into ukg.json_employee_pay_info (id,response) values ({0},'{1}');
                         """.format(the_id, data)
                        pg_cur_2.execute(sql)
                pg_con.commit()  # to capture ukg.json_employee_pay_info when there is error data
                pg_cur.execute('select ukg.ext_employee_pay_info()')


class GetEmployeeProfiles(luigi.Task):
    """
    01/30/2022: scraping data for active employees only.
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetEmployeeAccrualBalances()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                id_array = []
                pg_cur.execute('truncate ukg.json_employee_profiles')
                sql = """
                    select employee_id 
                    from ukg.ext_employees;                
                """
                pg_cur.execute(sql)
                for row in pg_cur.fetchall():
                    id_array.append(str(row[0]))
                for the_id in id_array:
                    time.sleep(1)
                    print(the_id)
                    url = 'https://beta.rydellvision.com:8888/ukg/employees/' + the_id + '/profiles'
                    the_response = requests.request('GET', url)
                    data = the_response.text
                    data = data.replace("'", "''")
                    with pg_con.cursor() as pg_cur_2:
                        sql = """
                             insert into ukg.json_employee_profiles (id,response) values ({0},'{1}');
                         """.format(the_id, data)
                        pg_cur_2.execute(sql)
                    pg_con.commit()
                pg_cur.execute('select ukg.ext_employee_profiles()')
                pg_cur.execute('select ukg.update_employee_profiles()')


class GetEmployeeAccrualBalances(luigi.Task):
    """
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield UpdateEmployees()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                id_array = []
                pg_cur.execute('truncate ukg.json_employee_accrual_balances')
                sql = """
                    select employee_id 
                    from ukg.ext_employees;                
                """
                pg_cur.execute(sql)
                for row in pg_cur.fetchall():
                    id_array.append(str(row[0]))
                for the_id in id_array:
                    url = 'https://beta.rydellvision.com:8888/ukg/employees/' + the_id + '/accrual/1/balances'
                    the_response = requests.request('GET', url)
                    data = the_response.text
                    data = data.replace("'", "''")
                    with pg_con.cursor() as pg_cur_2:
                        sql = """
                             insert into ukg.json_employee_accrual_balances (id,response) values ({0},'{1}');
                         """.format(the_id, data)
                        pg_cur_2.execute(sql)


class GetPersonalRateTables(luigi.Task):
    """
    01/30/22 first night in luigi run, failed with lots of null rate_table_id
    don't know if it was bad data in ukg.ext_employee_profiles or missing rate tables
    given the past few nights where i get repeated errors due to "too many call" errors, just don't know
    so, rerun production luigi without making any changes (inner joins to generate ids ...),
    see what happens, go from there
    07/14/23 added where clause to base query, skip rows where rate_table_id is null
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetEmployeeProfiles()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                id_array = []
                pg_cur.execute('truncate ukg.json_personal_rate_tables')
                sql = """
                    select (c->>'id')::bigint as rate_table_id
                    from ukg.ext_employee_profiles a
                    left join jsonb_array_elements(a.rate_tables) b on true
                      and b->>'index' = '0'
                    left join jsonb_array_elements(b->'refs') c on true
                     where c->>'id' is not null;                
                """
                pg_cur.execute(sql)
                pg_con.commit()
                for row in pg_cur.fetchall():
                    id_array.append(str(row[0]))
                for the_id in id_array:
                    url = 'https://beta.rydellvision.com:8888/ukg/rates-tables/' + the_id + '/schedules'
                    the_response = requests.request('GET', url)
                    data = the_response.text
                    data = data.replace("'", "''")
                    time.sleep(1)
                    with pg_con.cursor() as pg_cur_2:
                        sql = """
                             insert into ukg.json_personal_rate_tables (id,response) values ({0},'{1}');
                         """.format(the_id, data)
                        pg_cur_2.execute(sql)
                        pg_con.commit()
                pg_cur.execute('select ukg.update_personal_rate_tables()')


# ---------------------------------------------------------------------
# Cost Centers
# ---------------------------------------------------------------------
class GetCostCenters(luigi.Task):
    """
    truncates and repopulates ukg.ext_cost_centers nightly
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_cost_centers')
                for idx in range(0, 9):
                    url = 'https://beta.rydellvision.com:8888/ukg/cost-centers?tree_index=' + str(idx)
                    the_response = requests.request('GET', url)
                    data = the_response.text
                    data = data.replace("'", "''")
                    with pg_con.cursor() as pg_cur_2:
                        sql = """
                            insert into ukg.json_cost_centers (cost_center_index,response) values ({0},'{1}');
                        """.format(idx, data)
                        pg_cur_2.execute(sql)
                        pg_cur.execute('select ukg.ext_cost_centers()')


class GetCostCenterJobs(luigi.Task):
    """
    truncates and repopulates ukg.json_cost_center_jobs nightly
    there is no ukg.ext_cost_center_jobs, no unique data, all that is needed is the id to get cost_center_details

    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = 'https://beta.rydellvision.com:8888/ukg/cost-center-jobs'
        the_response = requests.request('GET', url)
        data = the_response.text
        data = data.replace("'", "''")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_cost_center_jobs')
                sql = """
                    insert into ukg.json_cost_center_jobs (response) values ('{0}');
                """.format(data)
                pg_cur.execute(sql)


class GetCostCenterJobDetails(luigi.Task):
    """
    truncates and repopulates ukg.json_cost_center_jobdetails nightly
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetCostCenterJobs()
        yield GetEmployeePayInfo()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                id_array = []
                pg_cur.execute('truncate ukg.json_cost_center_job_details')
                pg_con.commit()
                sql = """
                    select b->'id'
                    from ukg.json_cost_center_jobs a
                    join jsonb_array_elements(a.response->'costCenterJobs') b on true;            
                """
                pg_cur.execute(sql)
                for row in pg_cur.fetchall():
                    id_array.append(str(row[0]))
                for the_id in id_array:
                    url = 'https://beta.rydellvision.com:8888/ukg/cost-center-jobs/' + the_id
                    the_response = requests.request('GET', url)
                    data = the_response.text
                    data = data.replace("'", "''")
                    time.sleep(1)
                    with pg_con.cursor() as pg_cur_2:
                        sql = """
                            insert into ukg.json_cost_center_job_details (id,response) values ({0},'{1}');
                        """.format(the_id, data)
                        pg_cur_2.execute(sql)
                        pg_con.commit()
                pg_cur.execute('select ukg.ext_cost_center_job_details();')


# ---------------------------------------------------------------------
# Misc
# ---------------------------------------------------------------------
class UpdateCommissionPayGroups(luigi.Task):
    """
    truncates and repopulates ukg.commission_pay_groups nightly
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetCostCenterJobDetails()
        yield GetEmployeePayInfo()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select ukg.update_commission_pay_groups();')


# ---------------------------------------------------------------------
# Payrolls
# ---------------------------------------------------------------------
class Payrolls(luigi.Task):
    """
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetCostCenterJobDetails()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = 'https://beta.rydellvision.com:8888/ukg/payrolls'
        the_response = requests.request('GET', url)
        data = the_response.text
        data = data.replace("'", "''")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_payrolls;')
                sql = """
                    insert into ukg.json_payrolls (response) values ('{0}');
                """.format(data)
                pg_cur.execute(sql)
                pg_con.commit()
                pg_cur.execute('select ukg.update_payrolls();')


class PayStatements(luigi.Task):
    """
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield Payrolls()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                id_array = []
                # pg_cur.execute('truncate ukg.json_pay_statements')
                sql = """
                    delete
                    from ukg.json_pay_statements
                    where id in (
                      select payroll_id
                      from ukg.payrolls
                      where payroll_start_date between current_date - 40 and current_date);
                """
                pg_cur.execute(sql)
                sql = """
                    select distinct payroll_id 
                    from ukg.payrolls
                    where payroll_start_date between current_date - 40 and current_date;
                """
                pg_cur.execute(sql)
                for row in pg_cur.fetchall():
                    id_array.append(str(row[0]))
                for the_id in id_array:
                    url = 'https://beta.rydellvision.com:8888/ukg/payrolls/' + the_id + '/pay-statements'
                    the_response = requests.request('GET', url)
                    data = the_response.text
                    data = data.replace("'", "''")
                    with pg_con.cursor() as pg_cur_2:
                        sql = """
                            insert into ukg.json_pay_statements (id,response) values ({0},'{1}');
                        """.format(the_id, data)
                        pg_cur_2.execute(sql)
                        pg_con.commit()
                pg_cur.execute('select ukg.update_pay_statements()')


class PayStatementDetails(luigi.Task):
    """
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield PayStatements()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                id_array = []
                # pg_cur.execute('truncate ukg.json_pay_statement_details')
                sql = """
                    delete
                    from ukg.json_pay_statement_details
                    where payroll_id in (
                      select payroll_id
                      from ukg.payrolls
                      where payroll_start_date between current_date - 40 and current_date);
                """
                pg_cur.execute(sql)
                sql = """
                    SELECT DISTINCT payroll_id, pay_statement_id 
                    from ukg.pay_statements
                    where payroll_id in (
                      select payroll_id 
                      from ukg.payrolls
                      where payroll_start_date between current_date - 40 and current_date);
                """
                pg_cur.execute(sql)
                for row in pg_cur.fetchall():
                    id_array.append([str(row[0]), str(row[1])])
                for item in id_array:
                    url = 'https://beta.rydellvision.com:8888/ukg/payrolls/' + item[0] + '/pay-statements/' + item[1]
                    the_response = requests.request('GET', url)
                    data = the_response.text
                    data = data.replace("'", "''")
                    time.sleep(1)
                    with pg_con.cursor() as pg_cur_2:
                        sql = """
                            insert into ukg.json_pay_statement_details 
                                (payroll_id, pay_statement_id,response) values ({0},{1},'{2}');
                        """.format(item[0], item[1], data)
                        pg_cur_2.execute(sql)


class PayrollStatementDetailTables(luigi.Task):
    """
    each of these functions parse ukg.json_pay_statement_details for the relevant data for each table
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield PayStatementDetails()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select ukg.update_pay_statement_earnings()')
                pg_cur.execute('select ukg.update_pay_statement_deductions()')
                pg_cur.execute('select ukg.update_pay_statement_taxes()')
                pg_cur.execute('select ukg.update_pay_statement_messages()')
                pg_cur.execute('select ukg.update_pay_statement_payments()')


class PayrollCDC(luigi.Task):
    """
    """
    pipeline = pipeline

    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield PayrollStatementDetailTables()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select * from ukg.payroll_cdc();"
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 0:
                    return requests.post(
                        "https://api.mailgun.net/v3/notify.cartiva.com/messages",
                        auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
                        data={"from": "jandrews@cartiva.com <luigi@notify.cartiva.com>",
                              "to": ["jandrews@cartiva.com"],
                              "subject": "PayrollCDC detected changed data",
                              "text": "PayrollCDC detected changed data"})


class Ukg(luigi.WrapperTask):
    """
    a wrapper task that invokes all the classes in this module
    this task is susequently listed as a requirement in run_all
    that way i don't need to construct atrificial dependencies just to ensure everything gets run
    """
    pipeline = pipeline

    def requires(self):
        yield GetJobs()
        yield GetEarningsCodes()
        yield GetPayCalcProfiles()
        yield GetTimeEntries()
        yield XfmClockHours()
        yield UpdateCommissionPayGroups()
        yield PayrollCDC()


if __name__ == '__main__':
    luigi.run(["--workers=4"], main_task_cls=GetEmployeeProfiles)
