# encoding=utf-8
"""
12/19/21
damnit, the time entries faild again with: {"errors": [{"code": 401, "message": "The access token invalid"}],
            "user_messages": [{"text": "The access token invalid", "details": {"code": 401}, "severity": "ERROR"}]}
but, now  if the_response.status_code != 200:
            raise Exception('Bad Response')
does not throw the error
but if i do this:
        the_response = requests.request('GET', url)
        the_response.status_code = 401
        if the_response.status_code != 200:
            raise Exception('Bad Response')
        data = the_response.text
it fucking fails like it should
double checked the code on production, it is what it should be
ran the above forced test on production and it failed
but the fucking 9:00 populated ukg.json_today_time_entries with the above error message but still sent out and
email which WAS FUCKING WRONG
have to put the test in the stored proc
disabled the cron job on the production server

in FUNCTION ukg.ext_today_time_entries(), tested ukg.json_today_time_entries for '%error%', if so,
raise an exception, appears to work ok
"""
import luigi
import utilities
import datetime
import csv
import requests
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email import encoders
import os
import smtplib

pipeline = 'today_clock_hours'
db2_server = 'report'
pg_server = '173'
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())


@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_with_minute, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_with_minute, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class ExtPypclockinToday(luigi.Task):
    """
    extracts current day data from pypclockin
    """
    extract_file = '../../extract_files/pypclockin_today.csv'
    pipeline = pipeline

    def local_target(self):
        return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                      TRIM(YICO#),TRIM(PYMAST_EMPLOYEE_NUMBER),YICLKIND,
                      case when yiclkint = '24:00:00' then '23:59:59' else yiclkint end as yiclkint,
                      YICLKOUTD,
                      case when yiclkoutt = '24:00:00' then '23:59:59' else yiclkoutt end as YICLKOUTT,
                      TRIM(YISTAT),
                      TRIM(YIINFO),TRIM(YICODE),TRIM(YIUSRIDI),TRIM(YIWSIDI),TRIM(YIUSRIDO),TRIM(YIWSIDO)
                    from rydedata.pypclockin
                    where yico# in ('RY1','RY2')
                      and yiclkind = curdate()"""
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:

                sql = "truncate arkona.ext_pypclockin_today"
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pypclockin_today from stdin with csv encoding 'latin-1 '""", io)
                pg_cur.execute('select arkona.update_today_clock_times()')
                pg_cur.execute('select arkona.update_today_clock_hours()')


class GetTodayTimeEntries(luigi.Task):
    """
    initially using ts_with_minute for local_target
    """
    pipeline = pipeline
    the_date = datetime.date.today()

    def local_target(self):
        return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = ('https://beta.rydellvision.com:8888/ukg/time-entries?start_date='
               + str(self.the_date) + '&end_date=' + str(self.the_date))
        the_response = requests.request('GET', url)
        # the_response.status_code = 401
        # if the_response.status_code != 200:
        #     raise Exception('Bad Response')
        data = the_response.text
        data = data.replace("'", "''")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate ukg.json_today_time_entries')
                sql = """
                    insert into ukg.json_today_time_entries (response) values ('{0}');
                """.format(data)
                pg_cur.execute(sql)
                pg_con.commit()
                pg_cur.execute('select ukg.ext_today_time_entries()')
                pg_cur.execute('select ukg.update_today_clock_times()')
                pg_cur.execute('select ukg.update_today_clock_hours()')


class EmailFile(luigi.Task):
    """
    """
    pipeline = pipeline
    extract_file = '../../extract_files/today_compare_clock_hours.csv'

    def local_target(self):
        return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        yield ExtPypclockinToday()
        yield GetTodayTimeEntries()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                with open(self.extract_file, 'w') as f:
                    writer = csv.writer(f)
                    pg_cur.execute("select * from ukg.compare_today_clock_hours();")
                    writer.writerows(pg_cur)
        comma_space = ', '
        sender = 'jandrews@cartiva.com'
        recipients = ['jandrews@cartiva.com', 'test@cartiva.com']
        # recipients = ['jandrews@cartiva.com', 'jgardner@rydellcars.com']
        outer = MIMEMultipart()
        outer['Subject'] = 'Compare Clock Hours'
        outer['To'] = comma_space.join(recipients)
        outer['From'] = sender
        attachments = [self.extract_file]
        for x_file in attachments:
            try:
                with open(x_file, 'rb') as fp:
                    msg = MIMEBase('application', "octet-stream")
                    msg.set_payload(fp.read())
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(x_file))
                outer.attach(msg)
            except Exception:
                raise
        composed = outer.as_string()
        s = smtplib.SMTP('mail.cartiva.com')
        s.sendmail(sender, recipients, composed)
        s.close()


class TodayClockHours(luigi.WrapperTask):
    """
    """
    pipeline = pipeline

    def requires(self):
        yield ExtPypclockinToday()
        yield GetTodayTimeEntries()
        yield EmailFile()


if __name__ == '__main__':
    luigi.run(["--workers=2"], main_task_cls=EmailFile)
