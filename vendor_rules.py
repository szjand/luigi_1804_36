# encoding=utf-8
"""
4/10/18 of, starting off with a major mind fuck tar baby.
ideally, the dependency chain should include the compnent parts of fact_fs, sypffxmst, ffpxrefdta, etc
to correctly verify that requisity line_labels and semi_fixed_accounts are current and complete
i am not there yet, guess i will make it a TODO

"""

import luigi
import utilities
import datetime
import fact_gl
import ext_arkona

pipeline = 'vendor_rules'
db2_server = 'report'
pg_server = '173'


class Vendors(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield ext_arkona.ExtGlpcust()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select jeri.update_vendors()"""
                pg_cur.execute(sql)


class VendorSpendDetail(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield fact_gl.FactGl()
        yield Vendors()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select jeri.update_vendor_spend_detail()"""
                pg_cur.execute(sql)


class RunVendorRules(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield VendorSpendDetail()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select nrv.run_vendor_rules()"""
                pg_cur.execute(sql)


if __name__ == '__main__':
    luigi.run(["--workers=4"], main_task_cls=RunVendorRules())
