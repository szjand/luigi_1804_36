# encoding=utf-8
"""
08/26/22
  based on sc_payroll_201803.py and car_deals.py, going to try to combine them all into i file
"""
# TODO AccountsRoutes factory_financial_year is hard coded

import luigi
import utilities
import datetime
import csv
import car_deals
import fact_gl
import ukg
import fi_manager_comp

pipeline = 'toyota_sales_consultant_payroll'
db2_server = 'report'
pg_server = '173'


class AccountsRoutesTy(luigi.Task):
    """
    3/20/18: include page 7 lines 3, 13 - chargebacks
    """
    # TODO hard coded year in select from eisglobal.sypffxmst
    extract_file = '../../extract_files/ty_accounts_routes.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select b.company_number, a.fxmpge as the_page, integer(a.fxmlne) as line,
                      trim(b.g_l_acct_number) as gl_account
                    from (
                        select *
                        from eisglobal.sypffxmst
                        where fxmcyy = 2022
                          and trim(fxmcde) = 'GM'
                          and (
                            (fxmpge between 5 and 15)
                            or
                            (fxmpge = 16 and fxmlne between 1 and 14)
                            or
                            (fxmpge = 17 and fxmlne in (1,2,3,6,7,11,12,13,16,17)))) a
                    join (
                        select company_number, g_l_acct_number, factory_account
                        from rydedata.ffpxrefdta
                        where trim(company_number) = 'RY8'
                          and trim(factory_code) = 'GM'
                          and factory_financial_year  = 2022
                          and g_l_acct_number <> '') b on a.fxmact = b.factory_account
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate sls_toy.deals_accounts_routes")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert("""copy sls_toy.deals_accounts_routes from 
                    stdin with csv encoding 'latin-1 '""", io)


class DealsAccountingDetailTy(luigi.Task):
    """
    replaced sql in code with call to function
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield fact_gl.FactGl()
        yield AccountsRoutesTy()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from sls_toy.update_deals_accounting_detail()"""
                pg_cur.execute(sql)


class DealsGrossByMonthTy(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield DealsAccountingDetailTy()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from sls_toy.update_deals_gross_by_month()"""
                pg_cur.execute(sql)


class FiChargebacks(luigi.Task):
    """
    requires: used DealsByMonth because that requires both sls.deals & ads.ext_ads_dim_customer
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield DealsAccountingDetailTy()
        yield DealsByMonthTy()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from sls.update_fi_chargebacks()"""
                pg_cur.execute(sql)


if __name__ == '__main__':
    luigi.run(["--workers=4"], main_task_cls=DealsGrossByMonthTy)
