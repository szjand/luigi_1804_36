# encoding=utf-8
"""
move financial statement requirements into luigi
sypfflout, spyffxmst, ffpxrefdta have been run nightly on 22 in the task scheduler folder fin_data_mart
"""
import utilities
import luigi
import datetime
import csv

pipeline = 'fact_fs'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


class ExtFfpxrefdta(luigi.Task):
    """
    extract ffpxrefdta in to ext_ffpxrefdta_tmp
    compare that to ext_ffpxrefdta and notify if their are differences
    """
    pipeline = pipeline
    extract_file = '../../extract_files/ffpxrefdta.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as db2_con:
            with db2_con.cursor() as db2_cur:
                sql = """
                    select TRIM(COMPANY_NUMBER),TRIM(CONSOLIDATION_GRP),TRIM(G_L_COMPANY_),TRIM(FACTORY_CODE),
                        FACTORY_FINANCIAL_YEAR,TRIM(G_L_ACCT_NUMBER),TRIM(FACTORY_ACCOUNT),TRIM(FACTORY_ACCOUNT2),
                        TRIM(FACTORY_ACCOUNT3),TRIM(FACTORY_ACCOUNT4),TRIM(FACTORY_ACCOUNT5),TRIM(FACTORY_ACCOUNT6),
                        TRIM(FACTORY_ACCOUNT7),TRIM(FACTORY_ACCOUNT8),TRIM(FACTORY_ACCOUNT9),TRIM(FACTORY_ACCOUNT10),
                        FACT_ACCOUNT_,FACT_ACCOUNT__FXFP02,FACT_ACCOUNT__FXFP03,FACT_ACCOUNT__FXFP04,
                        FACT_ACCOUNT__FXFP05,FACT_ACCOUNT__FXFP06,FACT_ACCOUNT__FXFP07,FACT_ACCOUNT__FXFP08,
                        FACT_ACCOUNT__FXFP09,FACT_ACCOUNT__FXFP10
                    from rydedata.ffpxrefdta
                    where consolidation_grp  <> '3'
                        and factory_code = 'GM'
                        and trim(factory_account) <> '331A'
                        and factory_financial_year > 2010
                        and company_number in ('RY1', 'RY8')
                        and g_l_acct_number <> '' -- eliminates the "XFCOPY" accounts
                """
                db2_cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(db2_cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_ffpxrefdta_tmp")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_ffpxrefdta_tmp from stdin with csv encoding 'latin-1 '""", io)
                pg_con.commit()
                sql = """
                    select count(*)
                    from ((
                        select *
                        from arkona.ext_ffpxrefdta d
                        except
                        select *
                        from arkona.ext_ffpxrefdta_tmp e)
                      union (
                        select *
                        from arkona.ext_ffpxrefdta_tmp f
                        except
                        select *
                        from arkona.ext_ffpxrefdta g)) x
                """
                pg_cur.execute(sql)
                the_count = pg_cur.fetchone()[0]
                if the_count != 0:
                    raise Exception('ffpxrefdta has changed')


class ExtSypfflout(luigi.Task):
    """
    extract sypfflout in to ext_sypfflout_tmp
    compare that to ext_sypfflout and notify if their are differences
    """
    pipeline = pipeline
    extract_file = '../../extract_files/sypfflout.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as db2_con:
            with db2_con.cursor() as db2_cur:
                sql = """
                    select trim(flcode), flcyy,flpage,flseq,flline,flflne,flflsq,trim(fldata),trim(flcont)
                    from eisglobal.sypfflout
                    where trim(flcode) = 'GM'
                      and flcyy > 2010
                """
                db2_cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(db2_cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_eisglobal_sypfflout_tmp")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_eisglobal_sypfflout_tmp from stdin with csv encoding 'latin-1 '""", io)
                pg_con.commit()
                sql = """
                    select count (1)
                    from ((
                        select *
                        from arkona.ext_eisglobal_sypfflout_tmp d
                        except
                        select *
                        from arkona.ext_eisglobal_sypfflout e)
                      union (
                        select *
                        from arkona.ext_eisglobal_sypfflout f
                        except
                        select *
                        from arkona.ext_eisglobal_sypfflout_tmp g)) x
                """
                pg_cur.execute(sql)
                the_count = pg_cur.fetchone()[0]
                if the_count != 0:
                    raise Exception('eisglobal_sypfflout has changed')


class ExtSypffxmst(luigi.Task):
    """
    extract sypfflout in to ext_sypffxmst_tmp
    compare that to ext_sypffxmst and notify if their are differences
    """
    pipeline = pipeline
    extract_file = '../../extract_files/sypffxmst.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as db2_con:
            with db2_con.cursor() as db2_cur:
                sql = """
                    select trim(fxmcde), fxmcyy,trim(fxmact),fxmpge,fxmlne,fxmcol,fxmstr
                    from eisglobal.sypffxmst
                    where trim(fxmcde) = 'GM'
                      and fxmcyy > 2010
                """
                db2_cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(db2_cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_eisglobal_sypffxmst_tmp")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_eisglobal_sypffxmst_tmp from stdin with csv encoding 'latin-1 '""", io)
                pg_con.commit()
                sql = """
                    select count (*)
                    from ((
                        select *
                        from arkona.ext_eisglobal_sypffxmst
                        except
                        select *
                        from arkona.ext_eisglobal_sypffxmst_tmp)
                      union (
                        select *
                        from arkona.ext_eisglobal_sypffxmst_tmp
                        except
                        select *
                        from arkona.ext_eisglobal_sypffxmst)) x
                """
                pg_cur.execute(sql)
                the_count = pg_cur.fetchone()[0]
                if the_count != 0:
                    raise Exception('eisglobal_sypffxmst has changed')


class FactFsWrapper(luigi.WrapperTask):
    """
    a wrapper task that invokes all the ext classes in this module
    this task is susequently listed as a requirement in run_all
    """
    pipeline = pipeline

    def requires(self):
        yield ExtFfpxrefdta()
        yield ExtSypfflout()
        yield ExtSypffxmst()
