# encoding=utf-8
"""
09/17/21
    can not get this to pass
    and the flow makes no sense, i step to the first order failing, ZRGDDF
    need to take it out of luigi to troubleshoot
    all html files are in global_connect/html_files
    i put a break point on pg_cur.execute:
            sql = "
                select jo.update_vehicle_order_events();
            "
            pg_cur.execute(sql)
      and it doesn't fucking stop there until after going thruough  a LOT of orders
      gets to  ZRGDDF stops and when i run it fails
psycopg2.errors.CardinalityViolation: ON CONFLICT DO UPDATE command cannot affect row a second time
HINT:  Ensure that no rows proposed for insertion within the same command have duplicate constrained values.
CONTEXT:  SQL function "update_vehicle_order_events" statement 1
    and i never see any data in jo.ext_vehicle_order_events
    i don't get why it doesn't "reach" that sql statement until that order number
09/18/21
    this version works
"""
import utilities
import csv
from pathlib import Path
from bs4 import BeautifulSoup

pg_server = '173'

def main():
    html_path = Path.home() / 'projects' / 'luigi_1804_36' / 'global_connect' / 'html_files'
    not_found_path = Path.home() / 'projects' / 'luigi_1804_36' / 'global_connect' / 'orders_not_found'
    html_to_csv = Path.home() / 'projects' / 'luigi_1804_36' / 'global_connect' / 'html_to_csv' / 'html.csv'
    html_files = (entry for entry in html_path.iterdir() if entry.is_file())
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            for file in html_files:
                order_number = file.stem
                print(order_number)
                page = open(str(file))
                soup = BeautifulSoup(page.read(), "lxml")
                new_rows = []
                # only those rows with a class of greengray or white, eg data
                for row in soup.find_all('tr', {'class': ["greengray", "white"]}):
                    new_rows.append([val.text.strip() for val in row.select('td[width!="4"]')[1:9]])
                for row in new_rows:
                    #  insert the order_number as the first element in the list
                    row.insert(0, order_number)
                with html_to_csv.open('w') as f:
                    writer = csv.writer(f)
                    writer.writerows(row for row in new_rows if len(row) > 1)
                with html_to_csv.open('r') as io:
                    pg_cur.copy_expert(
                        """copy jo.ext_vehicle_order_events from stdin with csv encoding 'latin-1 '""", io)
                pg_con.commit()
                sql = """
                    select jo.update_vehicle_order_events();
                """
                pg_cur.execute(sql)
                pg_con.commit()

if __name__ == '__main__':
    main()