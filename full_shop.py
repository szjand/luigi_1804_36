# encoding=utf-8
"""
03/12/20
    removed requirement: ext_scheduler_honda_appointments
08/02/2022
    removed dependency on ads.ext_fact_repair_order, added dependency on dds.fact_repair_order
"""
import luigi
import utilities
import datetime
import ext_arkona
import ukg
import fact_repair_order

pipeline = 'full_shop'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")

##############################################################
# if these task are being called via run_all, that task will handle all of this logging
##############################################################
# @luigi.Task.event_handler(luigi.Event.START)
# def task_start(self):
#     utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
#
#
# @luigi.Task.event_handler(luigi.Event.SUCCESS)
# def task_success(self):
#     utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
#     try:  # WrapperTest throws an AttributeError, see /Notes/issues
#         with self.output().open("w") as out_file:
#             # write to the output file
#             out_file.write('pass')
#     except AttributeError:
#         pass
##############################################################
# if these task are being called via run_all, that task will handle all of this logging
##############################################################


class BdrAppointmentsByDay(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                pg_cur.execute("select ops.check_for_ads_extract('ext_scheduler_appointments')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: ext_scheduler_appointments is missing')
                # pg_cur.execute("select ops.check_for_ads_extract('ext_scheduler_honda_appointments')")
                # if not pg_cur.fetchone()[0]:
                #     raise ValueError('The ads requirement: ext_scheduler_honda_appointments is missing')
                pg_cur.execute('select fs.update_bdr_appointments_by_day();')


class CsrAppointmentsByDay(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                pg_cur.execute("select ops.check_for_ads_extract('ext_bs_appointments')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: ext_scheduler_appointments is missing')
                pg_cur.execute('select fs.update_csr_appointments_by_day();')


class AdvisorRosByDay(luigi.Task):
    """
    removed dependency on ads.ext_fact_repair_order, added dependency on dds.fact_repair_order
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_repair_order.FactRepairOrder()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        """
        removed advantage dependency
        """
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select fs.update_advisor_ros_by_day();')


class TechFlagHoursByDay(luigi.Task):

    """
    08/02/22 removed dependency on ads.ext_fact_repair_order, added dependency on dds.fact_repair_order
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_repair_order.FactRepairOrder()
        yield ext_arkona.ExtSdpxtim()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select fs.update_tech_flag_hours_by_day();')


class ClockHoursByDay(luigi.Task):

    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ukg.XfmClockHours()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select fs.update_clock_hours_by_day();')


class FullShop(luigi.Task):

    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield CsrAppointmentsByDay()
        yield BdrAppointmentsByDay()
        yield AdvisorRosByDay()
        yield TechFlagHoursByDay()
        yield ClockHoursByDay()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select fs.update_personnel_daily_details();')
                pg_cur.execute('select fs.update_job_daily_totals();')
