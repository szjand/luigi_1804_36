# encoding=utf-8
"""
    download the invoice table from global connect BARS as a csv file
wfpm: wholesale floor plan memo
date ranges
08/21 - 09/20
09/21 - 10/20
if current date <= 20th
	the month is current month, eg this is the credits for the current month
	then range = previous month 21st and this month 20th
if current date >= 21st
	the month is next month, eg this is the credits for the next month
	then range = current month 21st and next month 20th

so, today, 9/11/21, the interest credits are for september and the range is 08/21 - 09/20
september credits will be displayed from 8/21 thru 9/30

this script generates the list of invoices for the given from and thru dates only
so, from 9/21 thru 9/30, the month period is october, the range is 9/21 - 10/20
the email will need to extract the interest credits from the existing data

09/19/21
    instead of getting invoice by vin and having to loop through multiple invoices,
    get the invoices by invoice number

3/21/22
after manually running gc_invoices_by_date_range.py and gc_invoices_download.py for many months, including
crossing over to a new year, finally ready, i believe to put them into luigi
"""
import luigi
from selenium import webdriver
from datetime import datetime
from dateutil.relativedelta import *
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import NoSuchElementException
import requests
import utilities
import csv
import os
import smtplib
from email.message import EmailMessage

pipeline = 'interest_credits'
pg_server = '173'
ts_no_spaces = str(datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.now())
user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None
# local_or_production = 'local'
local_or_production = 'production'

# get all possibly needed dates

dt = datetime.today()
prev_month = dt + relativedelta(months=-1)
next_month = dt + relativedelta(months=+1)
if dt.day < 21:
    # period of interest is prev_month/21 thru cur_month/20
    from_mm = prev_month.strftime("%m")
    from_dd = '21'
    from_yyyy = prev_month.strftime("%Y")
    thru_mm = dt.strftime("%m")
    thru_dd = '20'
    thru_yyyy = dt.strftime("%Y")
    the_month = dt.strftime("%B")
elif dt.day > 20:
    # period of interest is cur_month/21 thru next_month/20
    from_mm = dt.strftime("%m")
    from_dd = '21'
    from_yyyy = dt.strftime("%Y")
    thru_mm = next_month.strftime("%m")
    thru_dd = '20'
    thru_yyyy = next_month.strftime("%Y")
    the_month = next_month.strftime("%B")
    # period of interest is prev_month/21 thru cur_month/20
    from_mm_1 = prev_month.strftime("%m")
    from_dd_1 = '21'
    from_yyyy_1 = prev_month.strftime("%Y")
    thru_mm_1 = dt.strftime("%m")
    thru_dd_1 = '20'
    thru_yyyy_1 = dt.strftime("%Y")
    the_month_1 = dt.strftime("%B")


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class InvoiceMetadata(luigi.Task):
    """
    from barrs download all vehicle invoice metadata for specified date range(s)
    """
    pipeline = pipeline

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/interest_credits/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/interest_credits/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                                    '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)
        return driver

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        username_box = driver.find_element_by_id('IDToken1')
        password_box = driver.find_element_by_id('IDToken2')
        login_btn = driver.find_element_by_name('Login.Submit')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        WebDriverWait(driver, 20).until(
            ec.presence_of_element_located(('id', 'div-vin')))

    def run(self):
        """
        """
        driver = self.setup_driver()
        try:
            driver.get('https://barsrr.autopartners.net/main')
            self.login(driver)
            # click Document -> Vehicle Invoice
            driver.find_element_by_xpath(
                '/html/body/br-root/br-main/div/br-search-criteria/'
                'div/form/table/tr[2]/td[2]/select/option[10]').click()
            if dt.day < 21:
                # fill out the dates
                driver.find_element_by_xpath('//*[@id="div-smm"]').send_keys(from_mm)
                driver.find_element_by_xpath('//*[@id="div-sdd"]').send_keys(from_dd)
                driver.find_element_by_xpath('//*[@id="div-syyyy"]').send_keys(from_yyyy)
                driver.find_element_by_xpath('//*[@id="div-emm"]').send_keys(thru_mm)
                driver.find_element_by_xpath('//*[@id="div-edd"]').send_keys(thru_dd)
                driver.find_element_by_xpath('//*[@id="div-eyyyy"]').send_keys(thru_yyyy)
                # click the search button
                driver.find_element_by_xpath('//*[@id="search-form"]/form/section[2]/div[3]/input').click()
                time.sleep(5)
                # just the table body to csv
                # https://stackoverflow.com/questions/33633416/convert-html-table-to-csv-in-python
                tbl = driver.find_element_by_xpath('//*[@id="search-result"]/section[2]/div/table/tbody')
                with open(self.download_dir + 'metadata_1.csv', 'w', newline='') as csvfile:
                    wr = csv.writer(csvfile)
                    for row in tbl.find_elements_by_css_selector('tr'):
                        # this is a list comprehension
                        # comprehensions (inclusion) are apprpriate when the goal is to build a
                        # list by looping over an iterable (from an interation)
                        # adding the 'if' eliminates the 1st 2 empty columns
                        wr.writerow([d.text for d in row.find_elements_by_css_selector('td') if len(d.text) > 0])
                with utilities.pg(pg_server) as pg_con:
                    with pg_con.cursor() as pg_cur:
                        sql = """ truncate gmgl.wfpm_nightly_invoices_ext;"""
                        pg_cur.execute(sql)
                        with open(self.download_dir + 'metadata_1.csv', 'r') as io:
                            pg_cur.copy_expert(
                                """copy gmgl.wfpm_nightly_invoices_ext from stdin with csv encoding 'latin-1 '""", io)
            elif dt.day > 20:
                # need to get the invoices for 2 date ranges
                with utilities.pg(pg_server) as pg_con:
                    with pg_con.cursor() as pg_cur:
                        sql = """ truncate gmgl.wfpm_nightly_invoices_ext;"""
                        pg_cur.execute(sql)
                # fill out the dates
                driver.find_element_by_xpath('//*[@id="div-smm"]').send_keys(from_mm)
                driver.find_element_by_xpath('//*[@id="div-sdd"]').send_keys(from_dd)
                driver.find_element_by_xpath('//*[@id="div-syyyy"]').send_keys(from_yyyy)
                driver.find_element_by_xpath('//*[@id="div-emm"]').send_keys(thru_mm)
                driver.find_element_by_xpath('//*[@id="div-edd"]').send_keys(thru_dd)
                driver.find_element_by_xpath('//*[@id="div-eyyyy"]').send_keys(thru_yyyy)
                # this try except block is to catch when there are no invoices for the new period
                # on the early days of the new period, eg, on 11/22 there were no invoices for 11/21 -> 12/20
                try:
                    # click the search button
                    driver.find_element_by_xpath('//*[@id="search-form"]/form/section[2]/div[3]/input').click()
                    time.sleep(5)
                    # just the table body to csv
                    # https://stackoverflow.com/questions/33633416/convert-html-table-to-csv-in-python
                    tbl = driver.find_element_by_xpath('//*[@id="search-result"]/section[2]/div/table/tbody')
                    with open(self.download_dir + 'metadata_1.csv', 'w', newline='') as csvfile:
                        wr = csv.writer(csvfile)
                        for row in tbl.find_elements_by_css_selector('tr'):
                            # this is a list comprehension
                            # comprehensions (inclusion) are apprpriate when the goal is to build a
                            # list by looping over an iterable (from an interation)
                            # adding the 'if' eliminates the 1st 2 empty columns
                            wr.writerow([d.text for d in row.find_elements_by_css_selector('td') if len(d.text) > 0])
                    with utilities.pg(pg_server) as pg_con:
                        with pg_con.cursor() as pg_cur:
                            with open(self.download_dir + 'metadata_1.csv', 'r') as io:
                                pg_cur.copy_expert("""copy gmgl.wfpm_nightly_invoices_ext from stdin with csv 
                                encoding 'latin-1 '""", io)
                except NoSuchElementException:
                    pass
                # clear the date text boxes
                driver.find_element_by_xpath('//*[@id="div-smm"]').clear()
                driver.find_element_by_xpath('//*[@id="div-sdd"]').clear()
                driver.find_element_by_xpath('//*[@id="div-syyyy"]').clear()
                driver.find_element_by_xpath('//*[@id="div-emm"]').clear()
                driver.find_element_by_xpath('//*[@id="div-edd"]').clear()
                driver.find_element_by_xpath('//*[@id="div-eyyyy"]').clear()

                driver.find_element_by_xpath('//*[@id="div-smm"]').send_keys(from_mm_1)
                driver.find_element_by_xpath('//*[@id="div-sdd"]').send_keys(from_dd_1)
                driver.find_element_by_xpath('//*[@id="div-syyyy"]').send_keys(from_yyyy_1)
                driver.find_element_by_xpath('//*[@id="div-emm"]').send_keys(thru_mm_1)
                driver.find_element_by_xpath('//*[@id="div-edd"]').send_keys(thru_dd_1)
                driver.find_element_by_xpath('//*[@id="div-eyyyy"]').send_keys(thru_yyyy_1)
                # click the search button
                driver.find_element_by_xpath('//*[@id="search-form"]/form/section[2]/div[3]/input').click()
                time.sleep(5)
                # just the table body to csv
                tbl = driver.find_element_by_xpath('//*[@id="search-result"]/section[2]/div/table/tbody')
                with open(self.download_dir + 'metadata_2.csv', 'w', newline='') as csvfile:
                    wr = csv.writer(csvfile)
                    for row in tbl.find_elements_by_css_selector('tr'):
                        wr.writerow([d.text for d in row.find_elements_by_css_selector('td') if len(d.text) > 0])
                with utilities.pg(pg_server) as pg_con:
                    with pg_con.cursor() as pg_cur:
                        with open(self.download_dir + 'metadata_2.csv', 'r') as io:
                            pg_cur.copy_expert(
                                """copy gmgl.wfpm_nightly_invoices_ext from stdin with csv encoding 'latin-1 '""", io)
        finally:
            driver.quit()


class DownloadInvoices(luigi.Task):
    """
    from barrs download the invoices from InvoiceMetadata
    """
    pipeline = pipeline

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/interest_credits/invoice_downloads/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/interest_credits/invoice_downloads/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield InvoiceMetadata()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.download_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        username_box = driver.find_element_by_id('IDToken1')
        password_box = driver.find_element_by_id('IDToken2')
        login_btn = driver.find_element_by_name('Login.Submit')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        WebDriverWait(driver, 20).until(
            ec.presence_of_element_located(('id', 'div-vin')))

    def run(self):
        """
        """
        for filename in os.listdir(self.download_dir):
            os.unlink(self.download_dir + filename)
        driver = self.setup_driver()
        try:
            driver.get('https://barsrr.autopartners.net/main')
            self.login(driver)
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    pg_cur.execute("truncate gmgl.wfpm_ext_raw_invoices")
                    # this will be data from gmgl.wfpm_nightly_invoices_ext that does not yet exist
                    sql = """ 
                        select invoice_number 
                        from gmgl.wfpm_nightly_invoices_ext a
                        where not exists (  
                          select 1
                          from gmgl.wfpm_invoices
                          where invoice_number = a.invoice_number);
                    """
                    pg_cur.execute(sql)
                    for the_invoice_number in pg_cur.fetchall():
                        # click Document -> Vehicle Invoice
                        driver.find_element_by_xpath(
                            '/html/body/br-root/br-main/div/br-search-criteria/'
                            'div/form/table/tr[2]/td[2]/select/option[10]').click()
                        invoice_number_input = driver.find_element_by_xpath('//*[@id="div-docinvno"]')
                        # clear VIN text box
                        invoice_number_input.clear()
                        # enter vin into VIN text box
                        invoice_number_input.send_keys(the_invoice_number)
                        # click the search button
                        driver.find_element_by_name('submit').click()
                        # wait until table of results is located
                        WebDriverWait(driver, 5).until(ec.presence_of_element_located(('name', 'downloadImage')))
                        time.sleep(3)
                        # click the download button
                        driver.find_element_by_xpath(
                            '//*[@id="search-result"]/section[2]/div/table/tbody/tr/td[2]/img').click()
                        time.sleep(3)
        finally:
            driver.quit()

        with utilities.pg(pg_server) as pg_con:
            for file_name in os.listdir(self.download_dir):
                with open(self.download_dir + file_name, 'r') as text:
                    with pg_con.cursor() as pg_cur_2:
                        sql = """
                            insert into gmgl.wfpm_ext_raw_invoices values ('{0}')
                        """.format(text.read().replace("'", "''"))
                        pg_cur_2.execute(sql)


class UpdateInvoices(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield DownloadInvoices()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select gmgl.wfpm_update_invoices()")


class InterestCreditEmail(luigi.Task):
    """
    """
    pipeline = pipeline
    ddt = None
    from_mm = None
    from_dd = None
    from_yyyy = None
    thru_mm = None
    thru_dd = None
    thru_yyyy = None
    the_month = None
    from_mm_1 = None
    from_dd_1 = None
    from_yyyy_1 = None
    thru_mm_1 = None
    thru_dd_1 = None
    thru_yyyy_1 = None
    the_month_1 = None

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield UpdateInvoices()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        ddt = datetime.today()
        # dt_str = '03/21/2022'
        # ddt = datetime.strptime(dt_str, '%m/%d/%Y').date()
        prev_month_1 = ddt + relativedelta(months=-1)
        next_month_1 = ddt + relativedelta(months=+1)
        if ddt.day < 21:
            # period of interest is prev_month_1/21 thru cur_month/20
            self.from_mm = prev_month_1.strftime("%m")
            self.from_dd = '21'
            self.from_yyyy = prev_month_1.strftime("%Y")
            self.thru_mm = ddt.strftime("%m")
            self.thru_dd = '20'
            self.thru_yyyy = ddt.strftime("%Y")
            self.the_month = ddt.strftime("%B")
        elif ddt.day > 20:
            # period of interest is cur_month/21 thru next_month_1/20
            self.from_mm = ddt.strftime("%m")
            self.from_dd = '21'
            self.from_yyyy = ddt.strftime("%Y")
            self.thru_mm = next_month_1.strftime("%m")
            self.thru_dd = '20'
            self.thru_yyyy = next_month_1.strftime("%Y")
            self.the_month = next_month_1.strftime("%B")
            # period of interest is prev_month_1/21 thru cur_month/20
            self.from_mm_1 = prev_month_1.strftime("%m")
            self.from_dd_1 = '21'
            self.from_yyyy_1 = prev_month_1.strftime("%Y")
            self.thru_mm_1 = ddt.strftime("%m")
            self.thru_dd_1 = '20'
            self.thru_yyyy_1 = ddt.strftime("%Y")
            self.the_month_1 = ddt.strftime("%B")
        body = ''
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                if ddt.day < 21:
                    sql = """select * from gmgl.wfpm_interest_credit_email('{0}','{1}','{2}')
                    """.format(self.the_month + ' ' + self.from_yyyy,
                               from_mm + '/' + self.from_dd + '/' + self.from_yyyy,
                               self.thru_mm + '/' + self.thru_dd + '/' + self.thru_yyyy)
                    pg_cur.execute(sql)
                    for t in pg_cur.fetchall():
                        body += t[0] + '\n'
                elif ddt.day > 20:
                    sql = """select * from gmgl.wfpm_interest_credit_email('{0}','{1}','{2}','{3}','{4}','{5}')
                    """.format(self.the_month + ' ' + self.from_yyyy,
                               self.from_mm + '/' + self.from_dd + '/' + self.from_yyyy,
                               self.thru_mm + '/' + self.thru_dd + '/' + self.thru_yyyy,
                               self.the_month_1 + ' ' + self.from_yyyy_1,
                               self.from_mm_1 + '/' + self.from_dd_1 + '/' + self.from_yyyy_1,
                               self.thru_mm_1 + '/' + self.thru_dd_1 + '/' + self.thru_yyyy_1)
                    pg_cur.execute(sql)
                    for t in pg_cur.fetchall():
                        body += t[0] + '\n'
            return requests.post(
                "https://api.mailgun.net/v3/notify.cartiva.com/messages",
                auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
                data={"from": "jandrews@cartiva.com <jon@notify.cartiva.com>",
                      "to": ['jandrews@cartiva.com', 'gsorum@cartiva.com', 'nshirek@rydellcars.com',
                             'jschmiess@rydellcars.com', 'bknudson@rydellcars.com', 'abruggeman@cartiva.com',
                             'myem@rydellcars.com', 'bschumacher@rydellcars.com', 'bcahalan@rydellcars.com'],
                      "subject": 'Floorplan Interest Credits',
                      "text": body})

if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=InterestCreditEmail)
