
# encoding=utf-8
"""
dt_str = '03/19/2022'
dt = datetime.strptime(dt_str, '%m/%d/%Y').date()
prev_month = dt + relativedelta(months=-1)
next_month = dt + relativedelta(months=+1)
if dt.day < 21:
    # period of interest is prev_month/21 thru cur_month/20
    from_mm = prev_month.strftime("%m")
    from_dd = '21'
    from_yyyy = prev_month.strftime("%Y")
    thru_mm = dt.strftime("%m")
    thru_dd = '20'
    thru_yyyy = dt.strftime("%Y")
    the_month = dt.strftime("%B")
elif dt.day > 20:
    # period of interest is cur_month/21 thru next_month/20
    from_mm = dt.strftime("%m")
    from_dd = '21'
    from_yyyy = dt.strftime("%Y")
    thru_mm = next_month.strftime("%m")
    thru_dd = '20'
    thru_yyyy = next_month.strftime("%Y")
    the_month = next_month.strftime("%B")
    # period of interest is prev_month/21 thru cur_month/20
    from_mm_1 = prev_month.strftime("%m")
    from_dd_1 = '21'
    from_yyyy_1 = prev_month.strftime("%Y")
"""

from datetime import datetime
from dateutil.relativedelta import *
import utilities
import smtplib
from email.message import EmailMessage

# pipeline = 'interest_credits'
# pg_server = '173'
# ts_no_spaces = str(datetime.now()).replace(" ", "")
# ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.now())
# user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
# passcode = '666'
# username = None
# password = None
# local_or_production = 'local'
# # local_or_production = 'production'
#
# # get all possibly needed dates
# dt = datetime.today()
# # dt_str = '03/21/2022'
# # dt = datetime.strptime(dt_str, '%m/%d/%Y').date()
# prev_month = dt + relativedelta(months=-1)
# next_month = dt + relativedelta(months=+1)
# if dt.day < 21:
#     # period of interest is prev_month/21 thru cur_month/20
#     from_mm = prev_month.strftime("%m")
#     from_dd = '21'
#     from_yyyy = prev_month.strftime("%Y")
#     thru_mm = dt.strftime("%m")
#     thru_dd = '20'
#     thru_yyyy = dt.strftime("%Y")
#     the_month = dt.strftime("%B")
# elif dt.day > 20:
#     # period of interest is cur_month/21 thru next_month/20
#     from_mm = dt.strftime("%m")
#     from_dd = '21'
#     from_yyyy = dt.strftime("%Y")
#     thru_mm = next_month.strftime("%m")
#     thru_dd = '20'
#     thru_yyyy = next_month.strftime("%Y")
#     the_month = next_month.strftime("%B")
#     # period of interest is prev_month/21 thru cur_month/20
#     from_mm_1 = prev_month.strftime("%m")
#     from_dd_1 = '21'
#     from_yyyy_1 = prev_month.strftime("%Y")
#     thru_mm_1 = dt.strftime("%m")
#     thru_dd_1 = '20'
#     thru_yyyy_1 = dt.strftime("%Y")
#     the_month_1 = dt.strftime("%B")
#
# def main():
#     # if dt.day < 21:
#     #     print("select * from gmgl.wfpm_interest_credit_email(" +
#     #           the_month + ' ' + from_yyyy + ',' +
#     #           from_mm + '/' + from_dd + '/' + from_yyyy + ','  +
#     #           thru_mm + '/' + thru_dd + '/' + thru_yyyy + ')')
#     #
#     # elif dt.day > 20:
#     #     print("select * from gmgl.wfpm_interest_credit_email(" +
#     #           the_month + ' ' + from_yyyy + ',' +
#     #           from_mm + '/' + from_dd + '/' + from_yyyy + ','  +
#     #           thru_mm + '/' + thru_dd + '/' + thru_yyyy + ',' +
#     #           the_month_1 + ' ' + from_yyyy_1 + ',' +
#     #           from_mm_1 + '/' + from_dd_1 + '/' + from_yyyy_1 + ','  +
#     #           thru_mm_1 + '/' + thru_dd_1 + '/' + thru_yyyy_1 + ')')
#     body = ''
#     with utilities.pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             if dt.day < 21:
#                 sql = """select * from gmgl.wfpm_interest_credit_email('{0}','{1}','{2}')
#                 """.format(the_month + ' ' + from_yyyy,
#                            from_mm + '/' + from_dd + '/' + from_yyyy,
#                            thru_mm + '/' + thru_dd + '/' + thru_yyyy)
#                 pg_cur.execute(sql)
#                 for t in pg_cur.fetchall():
#                     body += t[0] + '\n'
#             elif dt.day > 20:
#                 sql = """select * from gmgl.wfpm_interest_credit_email('{0}','{1}','{2}','{3}','{4}','{5}')
#                 """.format(the_month + ' ' + from_yyyy,
#                            from_mm + '/' + from_dd + '/' + from_yyyy,
#                            thru_mm + '/' + thru_dd + '/' + thru_yyyy,
#                            the_month_1 + ' ' + from_yyyy_1,
#                            from_mm_1 + '/' + from_dd_1 + '/' + from_yyyy_1,
#                            thru_mm_1 + '/' + thru_dd_1 + '/' + thru_yyyy_1)
#                 pg_cur.execute(sql)
#                 for t in pg_cur.fetchall():
#                     body += t[0] + '\n'
#
#     msg = EmailMessage()
#     msg.set_content(body)
#     msg['Subject'] = 'Floorplan Interest Credits'
#     msg['From'] = 'jandrews@cartiva.com'
#     msg['To'] = 'jandrews@cartiva.com,gsorum@cartiva.com,nshirek@rydellcars.com,jschmiess@rydellcars.com,' \
#                 'bknudson@rydellcars.com,abruggeman@cartiva.com,myem@rydellcars.com,bcahalan@rydellcars.com'
#     # msg['To'] = 'jandrews@cartiva.com'
#     s = smtplib.SMTP('mail.cartiva.com')
#     s.send_message(msg)
#     s.quit()
#
#
# if __name__ == '__main__':
#     main()
#

import smtplib
from email.message import EmailMessage
extract_file = '../../extract_files/employee_list.csv'
msg = EmailMessage()
msg.set_content('dim tech requires attention')
msg['Subject'] = 'News from fact repair order - from luigi'
msg['From'] = 'webtrade@cartiva.com'
msg['To'] = 'jandrews@cartiva.com'
with open(extract_file, 'rb') as fp:
    msg.add_attachment(fp.read(), maintype='application', subtype='csv', filename='employee_list.csv')
with smtplib.SMTP_SSL('secure.emailsrvr.com', 465) as s:
    s.login('jandrews@cartiva.com', 'shotNevertissue_8*_')
    s.send_message(msg)


import requests
def send_simple_message():
    return requests.post(
        "https://api.mailgun.net/v3/notify.cartiva.com/messages",
        auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
        data={"from": "Excited User <afton@notify.cartiva.com>",
              "to": ["jandrews@cartiva.com"],
              "subject": "Hello",
              "text": "Testing some Mailgun awesomness!"})


send_simple_message()