# encoding=utf-8
"""
ts_with_minute: used for generating the output files, include hours & minutes to enable
running a task multiple times a day
04/03/2021
    afton updated the opentrack endpoints, resulting in changed outputs from the requests
    had to change the logic in generating the data for updating table gmgl.opentrack_update_audit

"""
import luigi
import utilities
import datetime
import json
import requests


pipeline = 'open_track'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())


# # if these task are being called via run_all, that task will handle all of this logging
# # or, as in this case, the logging is implemented in another invoking module, eg homenet_new

# @luigi.Task.event_handler(luigi.Event.START)
# def task_start(self):
#     utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
#
#
# @luigi.Task.event_handler(luigi.Event.SUCCESS)
# def task_success(self):
#     utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
#     try:  # WrapperTest throws an AttributeError, see /Notes/issues
#         with self.output().open("w") as out_file:
#             # write to the output file
#             out_file.write('pass')
#     except AttributeError:
#         pass


class UpdateNewVehiclePrices(luigi.Task):
    """
    invoked by homenet_new.ExtNewData()
    04/20/20: added nissan to the updating of prices in dealertrack
    """
    pipeline = pipeline
    url = 'https://beta.rydellvision.com:8888/vision/inventory-updates'
    
    def local_target(self):
        return ('../../luigi_output_files/' + ts_with_minute + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):    
        with utilities.pg(pg_server) as database:
            with database.cursor() as pg_cursor:
                sql = """truncate nc.ext_price_changes;"""
                pg_cursor.execute(sql)
                sql = """
                    insert into nc.ext_price_changes
                    select vin, coalesce(best_price, 0)::integer as best_price,
                    coalesce(advertised_price, 0)::integer as internet_price
                    from gmgl.get_vehicle_prices()
                    union
                    select vin, coalesce(best_price,0)::integer as best_price,
                    coalesce(ad_price,0)::integer  as internet_price
                    from  hn.get_nissan_prices()
                    union
                    select vin, coalesce(best_price,0)::integer as best_price,
                    coalesce(ad_price,0)::integer  as internet_price
                    from  hn.get_honda_prices()
                    except
                    select a.vin, a.best_price, a.internet_price
                    from nc.pricing_history a
                    where a.thru_ts > now();
                """
                pg_cursor.execute(sql)
                sql = """select * from nc.ext_price_changes;"""
                pg_cursor.execute(sql)
                for rows in pg_cursor.fetchall():
                    vin = rows[0]
                    best_price = rows[1]
                    internet_price = rows[2]
                    params = {
                        'vin': vin,
                        'internetPrice': internet_price,
                        'bestPrice': best_price}
                    r = requests.post(self.url, params)
                    with database.cursor() as second_pg_cursor:
                        if 'Failed' in json.dumps(r.json()):
                            response_json = r.json()['Errors']['Error']['Message']['_text']
                            insert_error_query = """
                                insert into gmgl.opentrack_update_audit values ('%s','%s','%s','%s',now(), '%s') 
                            """ % (vin, best_price, internet_price, False, json.dumps(response_json))
                            second_pg_cursor.execute(insert_error_query)
                        else:
                            response_json = r.json()['Message']['_text']
                            insert_success_query = """
                                insert into gmgl.opentrack_update_audit values ('%s','%s','%s','%s',now(), '%s') 
                            """ % (vin, best_price, internet_price, True, json.dumps(response_json))
                            second_pg_cursor.execute(insert_success_query)
                sql = """select nc.update_pricing_history();"""
                pg_cursor.execute(sql)
