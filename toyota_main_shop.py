# encoding=utf-8
"""
starting out with just Dayton's daily report of main shop flag hours
"""
import luigi
import utilities
import csv
import datetime
import requests
import misc

pipeline = 'toyota main shop'
pg_server = '173'


class DailyFlagHourReport(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield misc.UpdatePrsData()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        flag_hours_dir = 'toyota/'
        file_name = flag_hours_dir + 'flag_hours_daily_report.csv'
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                with open(file_name, 'w') as f:
                    writer = csv.writer(f)
                    pg_cur.execute('select * from ts.flag_hours_report_tech_header();')
                    writer.writerows(pg_cur)
                    pg_cur.execute('select * from ts.flag_hours_report_techs();')
                    writer.writerows(pg_cur)
                    writer.writerow([])
                    pg_cur.execute('select * from ts.flag_hours_report_shop_header();')
                    writer.writerows(pg_cur)
                    pg_cur.execute('select * from ts.flag_hours_report_shop();')
                    writer.writerows(pg_cur)
                    writer.writerow([])
                    pg_cur.execute('select * from ts.flag_hours_report_skill_header();')
                    writer.writerows(pg_cur)
                    pg_cur.execute('select * from ts.flag_hours_report_skill();')
                    writer.writerows(pg_cur)
        requests.post(
            "https://api.mailgun.net/v3/notify.cartiva.com/messages",
            auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
            files=[('attachment', ('main_shop_flag_hours.csv',
                                   open(flag_hours_dir + 'flag_hours_daily_report.csv', 'rb').read()))],
            data={"from": "jandrews@cartiva.com <jon@notify.cartiva.com>",
                  "to": ['jandrews@cartiva.com', 'dmarek@rydellcars.com'],
                  "subject": "Daily Main Shop Flag Hours Report",
                  "text": "Daily Main Shop Flag Hours Report"})


# if __name__ == '__main__':
#     luigi.run(["--workers=2"], main_task_cls=DailyFlagHourReport)
