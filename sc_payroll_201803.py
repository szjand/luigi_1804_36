# encoding=utf-8
"""
201709:
  tmpConsultants: add digital role to join on sls.personnel_roles
  ConsultantPayroll:  qualified from 200 -> 250
    when payplan = 'executive' then (250 * qual_count) + (280 * non_qual_count)
20180201
  uh oh PaidByMonth has hard coded year of 117
201803
  new pay plans
4/1/18 renamed from sales_consultant_payroll to sc_payroll_201803
03/01/22
    added ukg.XfmClockHours as a dependency for ClockHoursByMonth
04/01/22
    removed  PtoIntervalsDates, PtoIntervals & PaidByMonth
    added UpdateOpenMonth
09/05/22
    brought UpdateFiManagerComp to this module
    that now becomes the class to be called from RunAll
09/27/222
    removed class UpdateFiManagerComp, no longer doing the payroll for aubul & grollimund
    they are essentially salaried now
02/26/23
    while reformulating sales pay plans, discovered that AccountsRoutes, populating  sls.deals_accounts_routes was
    generating a lot of fixed routing for fixed and discrepancies in routing for deal accounts
    discovered the error in the query in AccountsRoutes and fixed it to generate routing for the GM Financial
    Statement only  see query \new_pay_plans_22302\new_pay_plans.sql
11/3/23
    removed the FiChargeback requirement for ConsultantPayroll
11/4/23
    holy crap, bunch of tasks in pipelines sales_consultant_payroll & car_deals  (DealsByMonth,DealsChangedRows,
    DealsNewRows,ExtAccountingDeals,ExtDeals,FiChargebacks,XfmDealsChangedRows,XfmDealsDeletedRows,
    XfmDealsNewRows) did not run, but run_all finished!!!
    guessing it is the requirement i removed yesterday, restore it today, see what happens tomorrow
    They make sense, since FIChargebacks() is never invoked, that would have triggered the preceding tasks

"""
# TODO AccountsRoutes factory_financial_year is hard coded

import luigi
import utilities
import datetime
import csv
import car_deals
import fact_gl
import ukg

pipeline = 'sales_consultant_payroll'
db2_server = 'report'
pg_server = '173'


class DealsByMonth(luigi.Task):
    """
    6/28/17: delete current month deals every night and reload
    04/03/18: replace sql in script with call to function
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield car_deals.DealsChangedRows()
        # yield AccountsRoutes()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                sql = """
                    select sum(the_count)
                    from (
                      select count(*) as the_count
                      from ops.ads_extract
                      where the_date = current_date
                        and task = 'ads_ext_dim_car_deal_info'
                        and complete = TRUE
                      union all
                      select count(*)
                      from ops.ads_extract
                      where the_date = current_date
                        and task = 'ads_ext_dim_vehicle'
                        and complete = TRUE
                      union all
                      select count(*)
                      from ops.ads_extract
                      where the_date = current_date
                        and task = 'ads_ext_dim_customer'
                        and complete = TRUE) a;
                """
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 3:
                    raise ValueError('The ads requirement is not there')
                sql = """select * from sls.update_deals_by_month()"""
                pg_cur.execute(sql)


class AccountsRoutes(luigi.Task):
    """
    3/20/18: include page 7 lines 3, 13 - chargebacks
    2/26/23: in the last line off the where clause, changed and trim(a.fxmcde) = 'TOY' to and trim(a.fxmcde) = 'GM'
    """
    # TODO hard coded year in select from eisglobal.sypffxmst
    extract_file = '../../extract_files/accounts_routes.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select store_code, fxmpge as the_page, INTEGER(fxmlne) as line, trim(g_l_acct_number) as gl_account
                    from (
                    select 
                      case
                        when b.g_l_company_ = 'RY1' and b.consolidation_grp = '2' then 'RY2'
                        when b.g_l_company_ = 'RY1' and b.consolidation_grp = '' then 'RY1'
                        when b.g_l_company_ = 'RY8' and b.consolidation_grp = '' then 'RY8'
                      end as store_code,
                      a.*, b.* -- a.fxmpge as the_page, INTEGER(a.fxmlne) as line, trim(b.g_l_acct_number) as gl_account
                    from eisglobal.sypffxmst a
                    inner join rydedata.ffpxrefdta b on a.fxmact = b.factory_account
                      and a.fxmcyy = b.factory_financial_year
                    where a.fxmcyy = 2023
                      and trim(a.fxmcde) in ('TOY', 'GM')
                      and b. consolidation_grp <> '3'
                      and b.factory_code in ('TOY', 'GM')
                      and trim(b.factory_account) <> '331A'
                      and b.company_number in ( 'RY1','RY8')
                      and b.g_l_acct_number <> ''
                      and (
                        (a.fxmpge between 5 and 15) or
                        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
                        -- exclude fin comp 
                        (a.fxmpge = 17 and a.fxmlne in (1,2,3,6,7,11,12,13,16,17)))
                    and ( -- this was necessary to get toyota
                      (b.g_l_company_ = 'RY1' and b.consolidation_grp = '2')
                      or
                      (b.g_l_company_ = 'RY1' and b.consolidation_grp = '')
                      or
                      (b.g_l_company_ = 'RY8' and b.consolidation_grp =  '' and trim(a.fxmcde) = 'GM'))) c
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate sls.deals_accounts_routes")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert("""copy sls.deals_accounts_routes from stdin with csv encoding 'latin-1 '""", io)


class DealsAccountingDetail(luigi.Task):
    """
    replaced sql in code with call to function
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield fact_gl.FactGl()
        yield AccountsRoutes()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from sls.update_deals_accounting_detail()"""
                pg_cur.execute(sql)


class DealsGrossByMonth(luigi.Task):
    """
    4/3/18: replace sql in script with call to function
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield DealsAccountingDetail()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from sls.update_deals_gross_by_month()"""
                pg_cur.execute(sql)


class FiChargebacks(luigi.Task):
    """
    requires: used DealsByMonth because that requires both sls.deals & ads.ext_ads_dim_customer
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield DealsAccountingDetail()
        yield DealsByMonth()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from sls.update_fi_chargebacks()"""
                pg_cur.execute(sql)


class ClockHoursByMonth(luigi.Task):
    """
    6/5/17 added the populating of sls.folks
    6/28/17: eliminated table sls.folks
    01/17/22 updated to use ukg, got rid of the query here, replaced with call to function
    03/1/22 need to make ukg.clock_hours a dependency, otherwise the sls.clock_hours is a day behind,
        eg, croaker and weber had pto time added on 2/28, but did not show up on the ukg import page
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield ukg.XfmClockHours()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select sls.update_clock_hours()"""
                pg_cur.execute(sql)


class ConsultantPayroll(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield FiChargebacks()
        yield DealsGrossByMonth()
        yield ClockHoursByMonth()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from sls.update_consultant_payroll()"""
                pg_cur.execute(sql)


class UpdateOpenMonth(luigi.Task):
    """
    the field sls.months.open_month use to be updated by mgrs submitting payroll, no more
    this is called from a separate cron job on the first of each month to update the open month
    needs to be run after all the updating done on the morning of the 1st, that is ensured
    by the requires function
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ConsultantPayroll()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select sls.update_open_month()"""
                pg_cur.execute(sql)


# if __name__ == '__main__':
#     luigi.run(["--workers=4"], main_task_cls=ConsultantPayroll)
