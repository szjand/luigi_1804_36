# encoding=utf-8
"""
the deal with not rerunning xfm tasks in luigi a second time is that once it has run successfully,
it does not need to run again, but being on a local dev machine,
it does not see the existence of the successful run
so it tries to change a row that has already been changed and we end up with a
thru date (current - 1) less than the from date (current)
real time will present its own issues

when this goes to production, the scrapers need to change back from current_date - 1 to current_date as the
current cut off

python 3 email is much simpler: https://docs.python.org/3/library/email.examples.html

because of the overlapping requirements of the Xfm tables, i will need to do the testing on
the production server
"""
import utilities
import luigi
import datetime
import ext_arkona
import xfm_arkona
import requests
import ukg


pipeline = 'fact_repair_order'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


class DimTechXfm(luigi.Task):
    """
    !!!!!!!!!!!!! don't rerun XfmPymast locally on the same day as the luigi run !!!!!!!!!!!!!!!!!!
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtSdptech()
        yield ukg.UpdateEmployees()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """ select dds.dim_tech_xfm() """
                pg_cur.execute(sql)


class DimTechEmail(luigi.Task):
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield DimTechXfm()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from dds.dim_tech_email()"""
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 0:
                    return requests.post(
                        "https://api.mailgun.net/v3/notify.cartiva.com/messages",
                        auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
                        data={"from": "jandrews@cartiva.com <luigi@notify.cartiva.com>",
                              "to": ["jandrews@cartiva.com"],
                              "subject": "dim tech requires attention",
                              "text": "dim tech requires attention"})


class DimServiceWriterXfm(luigi.Task):
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtSdpswtr()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """ select dds.dim_service_writer_xfm() """
                pg_cur.execute(sql)


class DimServiceWriterEmail(luigi.Task):
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield DimServiceWriterXfm()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from dds.dim_service_writer_email()"""
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 0:
                    return requests.post(
                        "https://api.mailgun.net/v3/notify.cartiva.com/messages",
                        auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
                        data={"from": "jandrews@cartiva.com <luigi@notify.cartiva.com>",
                              "to": ["jandrews@cartiva.com"],
                              "subject": "dim service writer requires attention",
                              "text": "dim service writer requires attention"})


class DimCustomerUpdate(luigi.Task):
    """
    !!!!!!!!!!!!! don't rerun XfmBopname locally on the same day as the luigi run !!!!!!!!!!!!!!!!!!1
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield xfm_arkona.XfmBopname()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """ select dds.dim_customer_update() """
                pg_cur.execute(sql)


class DimVehicleUpdate(luigi.Task):
    """
    !!!!!!!!!!!!! don't rerun XfmInpmast locally on the same day as the luigi run !!!!!!!!!!!!!!!!!!1
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield xfm_arkona.XfmInpmast()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """ select dds.dim_vehicle_update() """
                pg_cur.execute(sql)


class DimServiceTypeEmail(luigi.Task):
    """
    essentially a look up table, monitor for new service type
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtSdpsvct()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from dds.dim_service_type_email()"""
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 0:
                    return requests.post(
                        "https://api.mailgun.net/v3/notify.cartiva.com/messages",
                        auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
                        data={"from": "jandrews@cartiva.com <luigi@notify.cartiva.com>",
                              "to": ["jandrews@cartiva.com"],
                              "subject": "dim service type needs an intervention",
                              "text": "dim service type needs an intervention"})


class DimCCC(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtSdprtxt()
        yield ext_arkona.ExtPdpphdr()
        yield ext_arkona.ExtPdppdet()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """ select dds.dim_ccc_update() """
                pg_cur.execute(sql)


class DimRoComment(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtSdprtxt()
        yield ext_arkona.ExtPdpphdr()
        yield ext_arkona.ExtPdppdet()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """ select dds.dim_ro_comment_update() """
                pg_cur.execute(sql)


class DimOpcode(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtSdplopc()
        yield ext_arkona.ExtPdppdet()
        yield ext_arkona.ExtSdprdet()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """ select dds.dim_opcode_update() """
                pg_cur.execute(sql)


class FactRepairOrder(luigi.Task):
    pipeline = pipeline
    # extract_file = '../../extract_files/sypfflout.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtSdprhdr()
        yield ext_arkona.ExtSdprdet()
        yield ext_arkona.ExtPdpphdr()
        yield ext_arkona.ExtPdppdet()
        yield xfm_arkona.XfmBopname()
        yield DimTechEmail()
        yield DimServiceWriterEmail()
        yield DimCustomerUpdate()
        yield DimVehicleUpdate()
        yield DimServiceTypeEmail()
        yield DimCCC()
        yield DimRoComment()
        yield DimOpcode()
        yield ext_arkona.ExtVoidRos()
        yield ext_arkona.ExtSdprhdrCloseDates()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = " select dds.fact_repair_order_update() "
                pg_cur.execute(sql)
                sql = "select arkona.xfm_sdprhdr_close_dates()"
                pg_cur.execute(sql)
