# encoding=utf-8
"""
    runs on luigi server every 20 minutes 7 to 7 every day
    09/06/20 added select nrv.add_update_new_employees() to run
    09/28/20: due for a refactor, when there are overlapping requests, they fail the timeoff method due to PK
        violation, and therefore never get to the duplicate_request_email() method
    11/09/20: removed ExtPymast requirement
        been trying to figure out how to get rid of Cron Daemon emails generated when the cron job passes
        moved the task in the crontab above the MAILTO= command, mail not sent but gets generated as a file:
        /var/mail/rydell, added line at the end of this to empty that file, otherwise, this could be yet another
        "invisible" consumer of drive space
"""

import utilities
import luigi
import datetime
from selenium import webdriver
import time
import os
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import csv
import ext_arkona
import openpyxl
import smtplib
from email.message import EmailMessage

pipeline = 'compli'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())
username = 'jandrews@cartiva.com'
password = 'qcdfis5y'

local_or_production = 'production'
# local_or_production = 'local'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class CompliReports(luigi.Task):
    """
    substituted arkona.ext_pymast for dds.edwEmployeeDim in functions, employee dimension not done yet
    and arkona.xfm_pypclockin for dds.edwFactClockHours
    """
    pipeline = pipeline

    if local_or_production == 'local':
        working_dir = '/home/jon/projects/luigi_1804_36/compli/'
    elif local_or_production == 'production':
        working_dir = '/home/rydell/projects/luigi_1804_36/compli/'

    def local_target(self):
        return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.working_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def login(driver):
        # get username and password fields
        username_box = driver.find_element_by_id('txtLoginNM')
        password_box = driver.find_element_by_id('txtPassword')
        login_btn = driver.find_element_by_id('buttonLogin')
        username_box.send_keys(username)
        password_box.send_keys(password)
        login_btn.click()
        # Wait for the page to load after clicking the button
        WebDriverWait(driver, 20).until(ec.presence_of_element_located(('id', 'ctl00_ctl05_imageCompanyLogo')))

    def download_reports(self, driver):
        # Since we have successfully logged in go directly to the ExportUser report
        driver.get('https://secure.compli.com/Personnel/ExportUsers.asp')
        # go to the time allocation detail report
        driver.get('https://secure.compli.com/Managed/Administration/ReportViewer.aspx?reportId=34')
        # Include pending
        pending_id = 'ctl00_TopLeftNav_rptRptOptions_ctl00_chkOptionBoolean'
        driver.find_element_by_id(pending_id).click()
        # Extend the end date a year
        end_date_id = 'ctl00_TopLeftNav_rptRptOptions_ctl08_txtOptionDtm'
        end_date_box = driver.find_element_by_id(end_date_id)
        end_date = datetime.datetime.strptime(end_date_box.get_attribute('value'), "%m/%d/%Y")
        new_end_date = end_date + datetime.timedelta(days=365)
        end_date_box.clear()
        end_date_box.send_keys(datetime.datetime.strftime(new_end_date, "%m/%d/%Y"))
        # Include all users
        xpath = "//span[contains(.,'All Users')]/preceding-sibling::input[@type='checkbox']"
        # all_users_id = 'ctl00_TopLeftNav_rptRptOptions_ctl10_treeOptionListn47CheckBox'
        driver.find_element_by_xpath(xpath).click()
        # Refresh the report
        driver.find_element_by_id('ctl00_TopLeftNav_buttonExec').click()
        # Export to excel
        # everything on the Vehicle Status page is in an iframe named frmReportControl
        WebDriverWait(driver, 5).until(
            ec.frame_to_be_available_and_switch_to_it((By.XPATH, "//iframe[@id='frmReportControl']")))
        export_select_id = 'MSReportViewer_ctl01_ctl05_ctl00'
        export_select = driver.find_element_by_id(export_select_id)
        export_options = export_select.find_elements_by_tag_name("option")
        for option in export_options:
            if option.get_attribute("value") == "EXCELOPENXML":
                option.click()
                break
        # Click the export link
        export_link_id = 'MSReportViewer_ctl01_ctl05_ctl01'
        export_link = driver.find_element_by_id(export_link_id)
        export_link.click()
        # Give it a few seconds to download
        time.sleep(5)
        # Convert the xlsx to csv
        wb = openpyxl.load_workbook(self.working_dir + 'rptTimeOffTracker_TimeAllocationDetail.xlsx')
        sh = wb.active
        with open(self.working_dir + 'time_allocation_detail.csv', 'w', newline="") as f:
            c = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)
            for r in sh.rows:
                c.writerow([cell.value for cell in r])

    def users(self):
        file_name = self.working_dir + 'ExportUsers.csv'
        new_file_name = self. working_dir + 'export_users.csv'
        with open(file_name, 'r') as orig_file:
            with open(new_file_name, 'w') as mod_file:
                writer = csv.writer(mod_file)
                for row in csv.reader(orig_file):
                    if len(row) > 6:  # eliminate that annoying last line
                        writer.writerow(row)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate pto.tmp_compli_users")
                with open(new_file_name, 'r') as io:
                    pg_cur.copy_expert("""copy pto.tmp_compli_users from stdin with csv encoding 'latin-1 '""", io)
                sql = """select pto.compli_users_update();"""
                pg_cur.execute(sql)

    def timeoff(self):
        file_name = self.working_dir + 'time_allocation_detail.csv'
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate pto.tmp_compli_timeoff")
                with open(file_name, 'r') as io:
                    pg_cur.copy_expert("""copy pto.tmp_compli_timeoff from stdin with csv encoding 'latin-1 '""", io)
                sql = """select pto.requests_update();"""
                pg_cur.execute(sql)

    @staticmethod
    def duplicate_request_email():
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from pto.duplicate_request_email();"""
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 0:
                    msg = EmailMessage()
                    msg.set_content('duplicate time off request in compli, see function pto_duplicate_request_email()')
                    msg['Subject'] = 'duplicate time off request in compli'
                    msg['From'] = 'jandrews@cartiva.com'
                    msg['To'] = 'jandrews@cartiva.com'
                    s = smtplib.SMTP('mail.cartiva.com')
                    s.send_message(msg)
                    s.quit()

    def run(self):
        for filename in os.listdir(self.working_dir):
            os.unlink(self.working_dir + filename)
        driver = self.setup_driver()
        try:
            driver.get('https://secure.compli.com')
            self.login(driver)
            self.download_reports(driver)
            self.users()
            self.timeoff()
            self.duplicate_request_email()
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    pg_cur.execute('select nrv.add_update_new_employees();')
        finally:
            driver.quit()
        os.system('> /var/mail/rydell')


if __name__ == '__main__':
    luigi.run(["--workers=2"], main_task_cls=CompliReports)
