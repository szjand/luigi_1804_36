# encoding=utf-8
"""
data originates from .22 sched_tasks->vehicle_walk_feedback, runs @ 8,10,12,2,4 daily
this only generates emails if there is new walk data:
  for t in pg_cur.fetchall() does not process if there are no records returned
source: e:\python_projects\ext_ads\ext_vehicle_walk_feedback.py
this will be called from luigi @ 10 past each of those hours
11/22/24: replace the recipient list with a mailgun mailing list
    also noticed, for auction purchases, the previous owner is null, also coalesced the eval notes (in pg function)
    and changed the from from webtrade to jandrews@cartiva.com
"""
import utilities
import luigi
import datetime
import requests
import time

pipeline = 'parts'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())


class WalkFeedback(luigi.Task):
    """
    """
    extract_file = '../../extract_files/ext_new_data.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + ts_with_minute + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    # def the_email(self, seq, stock_number, the_body):
    #     return requests.post(
    #         "https://api.mailgun.net/v3/notify.cartiva.com/messages",
    #         auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
    #         data={"from": "webtrade" + seq + "@cartiva.com <jon@notify.cartiva.com>",
    #               "to": ["jandrews@cartiva.com", "gsorum@cartiva.com", "abruggeman@cartiva.com",
    #                      "bcahalan@rydellcars.com", "abbysorum@cartiva.com", "dwilkie@rydellcars.com",
    #                      "ndockendorf@rydellcars.com", "bknudson@rydellcars.com", "sfoster@rydellcars.com",
    #                      "gbeiter@rydellcars.com", "mlongoria@rydellcars.com", "nshirek@rydellcars.com",
    #                      "pclose@rydellcars.com", "tgrollimund@rydellcars.com", "amichael@rydellcars.com",
    #                      "jgreer@rydellcars.com", "cbach@rydellcars.com", "rstout@rydellcars.com",
    #                      "jolderbak@rydellcars.com", "bschumacher@rydellcars.com",
    #                      "ehanson@rydellcars.com", "ddubois@rydellcars.com"],
    #               "subject": "Walk Feedback: " + stock_number,
    #               "text": the_body})


    def the_email(self, seq, stock_number, the_body):
        return requests.post(
            "https://api.mailgun.net/v3/notify.cartiva.com/messages",
            auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
            data={"from": "jandrews@cartiva.com <jon@notify.cartiva.com>",
                  "to": "walk-feedback@notify.cartiva.com",
                  "subject": "Walk Feedback " + seq + ": " + stock_number,
                  "text": the_body})


    def run(self):
        """
        having trouble getting it to send all emails
        1. with the mail code in run, recvd only 1 of 11 potential emails
        2. with mail code in separate function
            a. recvd 3 of 11
            b. added 5 sec pause, recvd 9 of 11 over a period of 2 minutes
            c. added 30 sec pause,looks like all 11, but split between inbox and spam
            d. try sending from webrade@cartiva.com, with a 15 sec pause
            e. dynamically update from and subject, 20 sec pause
        """
        seq = 0
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                  select a.stock_number, (select * from ads.get_walk_feedback(a.stock_number))
                  from ads.ext_vehicle_walk_feedback a
                  where not sent
                    and walk_date = current_date;
                """
                pg_cur.execute(sql)
                for t in pg_cur.fetchall():
                    # print(t[0])
                    body = t[1]
                    seq = seq + 1
                    # print('seq: ' + str(seq))
                    sql = """
                        update ads.ext_vehicle_walk_feedback
                        set sent = true
                        where stock_number = '{0}';
                    """.format(t[0])
                    pg_cur.execute(sql)
                    time.sleep(20)
                    self.the_email(str(seq), t[0], body)


# if __name__ == '__main__':
#     luigi.run(["--workers=1"], main_task_cls=WalkFeedback)
