# encoding=utf-8
"""
01/11/23
    exclude ancient honda control 116744
    break out Honda and Nissan into separate rows at request of warranty dept
    runs every 10 minutes 8AM ato 4PM Monday thru Friday
"""
import luigi
import utilities
import csv
import datetime

pipeline = 'warranty_ros'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())
target_date = '{:%Y-%m-%d}'.format(datetime.datetime.now())

# local_or_production = 'local'
local_or_production = 'production'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class ExtWarrantyData(luigi.Task):
    """
    """

    pipeline = pipeline

    if local_or_production == 'local':
        extract_file = '/home/jon/projects/luigi_1804_36/warranty_schedule/ext_warranty_data.csv'
    elif local_or_production == 'production':
        extract_file = '/home/rydell/projects/luigi_1804_36/warranty_schedule/ext_warranty_data.csv'

    def local_target(self):
        return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select aa.store, trim(aa.control_number) as ro, trim(aa.account) as account, 
                      aa.balance, aa.description,
                      cast(left(digits(bb.final_close_date), 4) || '-' || 
                        substr(digits(bb.final_close_date), 5, 2) || '-' ||
                        substr(digits(bb.final_close_date), 7, 2)  as date) as close_date
                    from (
                      Select 'Toyota' as store, control_number, 
                        listagg(distinct trim(account_number), ' | ') as account, 
                        sum(transaction_amount) as balance,
                        listagg(distinct trim(description), ' | ') as description
                      from RYDEDATA.GLPTRNS 
                      where post_status = 'Y'
                        and trim(account_number) in ('2200','2255')
                        and trim(control_number) like '86%'
                      group by control_number
                        having sum(transaction_amount) <> 0
                      union
                      Select 'Honda' as store, control_number, account_number as account,
                        sum(transaction_amount) as balance,
                        listagg(distinct trim(description), ' | ') as description
                      from RYDEDATA.GLPTRNS
                      where post_status = 'Y'
                        and trim(account_number) = '226300'
                      group by control_number, account_number
                        having sum(transaction_amount) <> 0                       
                      union
                      Select 'Nissan' as store, control_number, account_number as account,
                        sum(transaction_amount) as balance,
                        listagg(distinct trim(description), ' | ') as description
                      from RYDEDATA.GLPTRNS
                      where post_status = 'Y'
                        and trim(account_number) = '226301'
                        and trim(control_number) not in ('116744')
                      group by control_number, account_number
                        having sum(transaction_amount) <> 0                        
                      union
                      Select 'GM' as store, control_number, account_number as account,
                        sum(transaction_amount) as balance,
                        listagg(distinct trim(description), ' | ') as description
                      from RYDEDATA.GLPTRNS
                      where post_status = 'Y'
                        and trim(account_number) = '126300'
                        and trim(control_number) not in ('117815')
                      group by control_number, account_number
                        having sum(transaction_amount) <> 0) aa
                    left join rydedata.sdprhdr bb on trim(aa.control_number) = trim(bb.ro_number)
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate nrv.ext_warranty_schedule")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy nrv.ext_warranty_schedule from stdin with csv encoding 'latin-1 '""", io)


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=ExtWarrantyData)
