# encoding=utf-8
"""
"""
import utilities
import luigi
import datetime
import fact_repair_order
import fact_gl
import xfm_arkona

pipeline = 'inventory_analysis'
pg_server = '173'

class InventoryDetail(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_repair_order.FactRepairOrder()
        yield fact_gl.FactGl()
        yield xfm_arkona.XfmInpmast()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirements
                pg_cur.execute("select ops.check_for_ads_extract('uc_inventory_ext_vehicle_inventory_items')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: uc_inventory_ext_vehicle_inventory_items is missing')
                pg_cur.execute("select ops.check_for_ads_extract('uc_inventory_ext_vehicle_items')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: uc_inventory_ext_vehicle_items is missing')
                pg_cur.execute("select ops.check_for_ads_extract('uc_inventory_ext_vehicle_inventory_item_statuses')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: uc_inventory_ext_vehicle_inventory_item_statuses is missing')
            with pg_con.cursor() as pg_cur1:
                sql = """
                    select the_date 
                    from dds.dim_date
                    where the_date = current_date - 1
                """
                pg_cur1.execute(sql)
                for row in pg_cur1.fetchall():
                    the_date = row[0]
                    with pg_con.cursor() as pg_cur2:
                        sql = """
                            select iar.update_inventory_detail('{}')
                        """.format(the_date)
                        pg_cur2.execute(sql)

# if __name__ == '__main__':
#     luigi.run(["--workers=4"], main_task_cls=InventoryDetail)