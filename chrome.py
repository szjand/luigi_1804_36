# encoding=utf-8
"""
Purpose is to build and maintain all possible configurations based on chrome style id

Model Years > 2017, chevrolet, buick, gmc, cadillac, honda, nissan only

in the describe vehicle section received the following error:
      File "/mnt/hgfs/E/python projects/ext_chrome/chrome.py", line 139, in <module>
            response = response.replace("'", "''")
        TypeError: a bytes-like object is required, not 'str'
changed the erroring line with:
    response = response.decode('unicode-escape').replace("'", "''")
5/28 one i got the script to work again, the above threw random psycopg2 invalid json errors
     so back to simple, without the encode/decode, and, today at least, it worked
12/8/19
    major refactoring: largely effective cdc
    to be run weekly, or at will
    initial implementation will be populating the jon schema, once it looks ok, move schema to chr that will require
        significant ddl modification

"""
import luigi
import utilities
import requests
import datetime


pipeline = 'chrome'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


# if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class Version(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        run_date = datetime.datetime.today().strftime('%m/%d/%Y')
        url = 'https://beta.rydellvision.com:8888/chrome/version-info'
        data = requests.get(url)
        data = data.text.replace("'", "''")  # .encode("utf-8")
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as table_cur:
                sql = """
                    insert into jon.get_version_info(the_date,response)
                    values ('{0}','{1}');
                """.format(run_date, data)
                table_cur.execute(sql)


class ModelYears(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield Version()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        url = 'https://beta.rydellvision.com:8888/chrome/model-years'
        data = requests.get(url)
        # insert the raw data into the get_table
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as chr_cur:
                chr_cur.execute("truncate jon.get_model_years")
                sql = """
                    insert into jon.get_model_years(model_years)
                    values ('{}');
                """.format(data.text)
                chr_cur.execute(sql)
                chr_cur.execute("select jon.xfm_model_years()")


class Divisions(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ModelYears()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as model_year_cur:
                sql = """
                    select model_year
                    from jon.model_years
                    where model_year > 2017;
                """
                model_year_cur.execute(sql)
                for model_year in model_year_cur:
                    url = 'https://beta.rydellvision.com:8888/chrome/divisions/' + str(model_year[0])
                    data = requests.get(url)
                    with pg_con.cursor() as chr_cur:
                        chr_cur.execute("truncate jon.get_divisions")
                        sql = """
                            insert into jon.get_divisions(model_year,divisions)
                            values ({0},'{1}');
                        """.format(model_year[0], data.text)
                        chr_cur.execute(sql)
                        chr_cur.execute("select jon.xfm_divisions()")


class ModelsByDivision(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield Divisions()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as get_models_cur:
                get_models_cur.execute("truncate jon.get_models_by_division")
            with pg_con.cursor() as division_cur:
                sql = """
                    select model_year, division_id
                    from jon.divisions
                    where division in('chevrolet','buick','gmc','cadillac','honda','nissan');
                """
                division_cur.execute(sql)
                for division in division_cur:
                    payload = {"year": division[0], "divisionId": division[1]}
                    url = 'https://beta.rydellvision.com:8888/chrome/models'
                    json_data = requests.post(url, data=payload)
                    with pg_con.cursor() as table_cur:
                        sql = """
                            insert into jon.get_models_by_division(model_year,division_id,models)
                            values ({0},{1},'{2}');
                        """.format(division[0], division[1], json_data.text)
                        table_cur.execute(sql)
            with pg_con.cursor() as models_cur:
                models_cur.execute("select jon.xfm_models()")


class Styles(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ModelsByDivision()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as get_styles_cur:
                get_styles_cur.execute("truncate jon.get_styles")
            with pg_con.cursor() as model_cur:
                model_sql = """
                    select model_id
                    from jon.models;
                """
                model_cur.execute(model_sql)
                for model_id in model_cur:
                    url = 'https://beta.rydellvision.com:8888/chrome/styles/' + str(model_id[0])
                    data = requests.get(url)
                    with pg_con.cursor() as table_cur:
                        sql = """
                            insert into jon.get_styles(model_id,styles)
                            values ({0},'{1}');
                        """.format(model_id[0], data.text)
                        table_cur.execute(sql)
            with pg_con.cursor() as styles_cur:
                styles_cur.execute("select jon.xfm_styles()")


class DescribeVehicleByStyleID(luigi.Task):
    """
    describe vehicle by style id with ShowAvailableEquipment
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield Styles()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as trun_cur:
                trun_cur.execute("truncate jon.get_describe_vehicle_by_style_id")
            with pg_con.cursor() as style_cur:
                style_sql = """
                    select chrome_style_id
                    from jon.styles;
                """
                style_cur.execute(style_sql)
                for stlye_id in style_cur:
                    url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-show-available/' + str(
                        stlye_id[0])
                    response = requests.get(url)
                    response = response.text.replace("'", "''")
                    with pg_con.cursor() as table_cur:
                        sql = """
                            insert into jon.get_describe_vehicle_by_style_id(chrome_style_id,response)
                            values ('{0}','{1}');
                        """.format(stlye_id[0], response)
                        table_cur.execute(sql)
            with pg_con.cursor() as style_cur:
                style_cur.execute("select jon.xfm_describe_vehicle_by_style_id()")


class Options(luigi.Task):
    """
    new rows and update change rows in options tables, eg configurations, packages, engines, etc
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield DescribeVehicleByStyleID()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select jon.xfm_configurations()")
                pg_cur.execute("select jon.xfm_engines()")
                pg_cur.execute("select jon.xfm_exterior_colors()")
                pg_cur.execute("select jon.xfm_interiors()")
                pg_cur.execute("select jon.xfm_transmissions()")
                pg_cur.execute("select jon.xfm_packages()")


if __name__ == '__main__':
    luigi.run(["--workers=4"], main_task_cls=Options)
