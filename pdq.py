# encoding=utf-8
"""
generate pdq writer stats for biweekly payroll
generate spreadsheet and send to the appropriate folks
everyday except monday at 4AM with previous day data, accumulated throughout the pay period
12/4
    per andrew's request added sales totals
02/25/20
    the weekly report to Ryan runs on Wednesdays at 4:55AM
05/15/2021
    the monthly report runs on the 1st of the month @ 5:05AM
11/02/21
    the weekly ry1 totals runs on sunday @ 6:00AM
05/30/22
    weekly and mtd now run on Monday mornings
"""
import luigi
import utilities
import csv
import datetime
from openpyxl import load_workbook
import openpyxl
import fact_repair_order
from dateutil.relativedelta import relativedelta
import requests

pipeline = 'pdq'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())

pdq_dir = 'pdq/'
file_name = pdq_dir + 'writer_stats.csv'
sos_file_name = pdq_dir + 'sos_writer_stats.csv'
weeky_file_name = pdq_dir + 'weekly_writer_stats.csv'
last_month_file_name = pdq_dir + 'last_month_stats.csv'
spreadsheet = pdq_dir + 'pdq_writer_stats.xlsx'
sos_spreadsheet = pdq_dir + 'sos_pdq_writer_stats.xlsx'
last_month_spreadsheet = pdq_dir + 'last_month_stats.xlsx'
weekly_spreadsheet = pdq_dir + 'weekly_writer_stats.xlsx'
mtd_file_name = pdq_dir + 'mtd_writer_stats.csv'
mtd_spreadsheet = pdq_dir + 'mtd_writer_stats.xlsx'
yesterday_file_name = pdq_dir + 'yesterday_stats.csv'
yesterday_spreadsheet = pdq_dir + 'yesterday_stats.xlsx'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class GetPdqRos(luigi.Task):
    """
    10/14/2020: removed the ads dependency, modified all functions to use dds.fact_repair_order (pg)
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_repair_order.FactRepairOrder()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select pdq.get_ros()')


class GetPdqRoSales(luigi.Task):
    """
    populate pdq.ro_sales
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetPdqRos()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select pdq.udpate_ro_sales()')


class CreatePdqWriterStats(luigi.Task):
    """
    create a spreadsheet of writer stats
    ads scrapes run after midnight, therefore this generates data for the
        previous day (current_date - 1)
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetPdqRoSales()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select 'Pay period from '::text ||
                      to_char(biweekly_pay_period_start_date, 'MM/DD/YYYY') || ' thru '::text ||
                      to_char(biweekly_pay_period_end_date, 'MM/DD/YYYY') || '      ' ||
                      'Data thru '::text || to_char(a.the_date, 'MM/DD/YYYY')
                    from dds.dim_date a
                    join dds.working_days b on a.the_date = b.the_date
                      and b.department = 'pdq'
                    where a.the_Date = current_date - 1;                   
                """
                pg_cur.execute(sql)
                with open(file_name, 'w') as f:
                    writer = csv.writer(f)
                    writer.writerow([pg_cur.fetchone()[0]])
                    writer.writerow([])
                    pg_cur.execute("select pdq.get_ro_sales_for_pay_period('{}')".format('RY1'))
                    writer.writerow(["RY1 Sales pay period to date: $" + str(pg_cur.fetchone()[0])])
                    pg_cur.execute("select pdq.get_ro_sales_for_pay_period('{}')".format('RY2'))
                    writer.writerow(["RY2 Sales pay period to date: $" + str(pg_cur.fetchone()[0])])
                    writer.writerow([])
                    writer.writerow(["store", "writer #", "writer", "emp #", "lof", "rotate", "air filters",
                                     "cabin filters", "batteries", "bulbs", "wipers", "lof pacing"])
                    pg_cur.execute('select * from pdq.get_writer_stats();')
                    writer.writerows(pg_cur)
                wb = openpyxl.Workbook()
                wb.save(spreadsheet)
                wb = load_workbook(spreadsheet)
                ws = wb.active
                with open(file_name) as f:
                    reader = csv.reader(f, delimiter=',')
                    for row in reader:
                        ws.append(row)
                wb.save(spreadsheet)


class EmailFile(luigi.Task):
    """
    based on python projects/open_ros/email_test.py
    mods:
        a single attachment
        smtp throws: AttributeError: SMTP instance has no attribute '__exit__'
            when invoked in a context manager, maybe a python 2 issue
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield CreatePdqWriterStats()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        return requests.post(
            "https://api.mailgun.net/v3/notify.cartiva.com/messages",
            auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
            files=[('attachment', ('pdq_writer_stats.xlsx', open(pdq_dir + 'pdq_writer_stats.xlsx', 'rb').read()))],
            data={"from": "jandrews@cartiva.com <jon@notify.cartiva.com>",
                  "to": ['jandrews@cartiva.com', 'rshroyer@gfhonda.com', 'aneumann@rydellcars.com',
                         'nneumann@rydellcars.com', 'bfraser@rydellcars.com', 'rsattler@rydellcars.com',
                         'gflaat@rydellcars.com', 'chosek@rydellcars.com', 'jstubb@rydellcars.com',
                         'dmarek@rydellcars.com','adecker@rydellcars.com'],
                  "subject": "PDQ Writer Stats",
                  "text": "Pay Period Stats"})


class LastMonthStoreStats(luigi.Task):
    """
    create a spreadsheet of store stats on the first of each month for the previous month
    make it dependent to EmailFile, thereby making sure all data is current
    initially mail only to Dayton & tyler

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield EmailFile()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        last_month = datetime.datetime.now() - relativedelta(months=1)
        last_month = format(last_month, '%B %Y')

        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                with open(last_month_file_name, 'w') as f:
                    writer = csv.writer(f)
                    pg_cur.execute("select * from pdq.get_writer_stats_for_last_month()")
                    writer.writerow(["Totals for" + last_month])
                    writer.writerow([])
                    writer.writerow(["store", "lof", "rotate", "air filters", "cabin filters", "batteries",
                                     "bulbs", "wipers"])
                    writer.writerows(pg_cur)
                wb = openpyxl.Workbook()
                wb.save(last_month_spreadsheet)
                wb = load_workbook(last_month_spreadsheet)
                ws = wb.active
                with open(last_month_file_name) as f:
                    reader = csv.reader(f, delimiter=',')
                    for row in reader:
                        ws.append(row)
                wb.save(last_month_spreadsheet)
        return requests.post(
            "https://api.mailgun.net/v3/notify.cartiva.com/messages",
            auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
            files=[('attachment', ('last_month_stats.xlsx', open(pdq_dir + 'last_month_stats.xlsx', 'rb').read()))],
            data={"from": "jandrews@cartiva.com <jon@notify.cartiva.com>",
                  "to": ['jandrews@cartiva.com', 'rshroyer@gfhonda.com', 'aneumann@rydellcars.com',
                         'bfraser@rydellcars.com'],
                  "subject": 'Cumulative Writer Stats for ' + last_month,
                  "text": "Last Month Stats"})


class WriterStatsWeekly(luigi.Task):
    """
    create a spreadsheet of writer stats every Sunday morning for the preceding week
    for Tyler (RY1) only
    05/30/22
        added honda nissan
        added penetration numbers
        changed date generation to run on Monday morning (rather than Sunday)
        changed name to WriterStatsWeekly
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield EmailFile()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        """
        changed the week of query to handle running on Monday morning rather than Sunday
        """
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select 'Week of '::text ||
                      (to_char(current_Date - 7, 'MM/DD/YYYY')) || ' thru '::text ||
                      (to_char(current_date - 1, 'MM/DD/YYYY'))                   
                """
                pg_cur.execute(sql)
                with open(weeky_file_name, 'w') as f:
                    writer = csv.writer(f)
                    writer.writerow([pg_cur.fetchone()[0]])
                    writer.writerow([])
                    writer.writerow(["store", "writer #", "writer", "emp #", "lof", "rotate", "air filters",
                                     "cabin filters", "batteries", "bulbs", "wipers"])
                    pg_cur.execute('select * from pdq.get_writer_stats_ry1_weekly();')
                    writer.writerows(pg_cur)
                wb = openpyxl.Workbook()
                wb.save(weekly_spreadsheet)
                wb = load_workbook(weekly_spreadsheet)
                ws = wb.active
                with open(weeky_file_name) as f:
                    reader = csv.reader(f, delimiter=',')
                    for row in reader:
                        ws.append(row)
                wb.save(weekly_spreadsheet)
        return requests.post(
            "https://api.mailgun.net/v3/notify.cartiva.com/messages",
            auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
            files=[('attachment', ('weekly_writer_stats.xlsx',
                                   open(pdq_dir + 'weekly_writer_stats.xlsx', 'rb').read()))],
            data={"from": "jandrews@cartiva.com <jon@notify.cartiva.com>",
                  "to": ['jandrews@cartiva.com', 'rshroyer@gfhonda.com', 'bfraser@rydellcars.com'],
                  "subject": "Weekly PDQ Writer Stats",
                  "text": "Weekly Stats"})


class WriterStatsMTD(luigi.Task):
    """
    MTD writer stats emailed out each monday
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield EmailFile()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        """
        """
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select distinct 'Month of '::text || month_name || ' ' || the_year::text 
                    from dds.dim_date 
                    where the_date = current_date;                 
                """

                pg_cur.execute(sql)
                with open(mtd_file_name, 'w') as f:
                    writer = csv.writer(f)
                    writer.writerow([pg_cur.fetchone()[0]])
                    writer.writerow([])
                    writer.writerow(["store", "writer #", "writer", "emp #", "lof", "rotate", "air filters",
                                     "cabin filters", "batteries", "bulbs", "wipers"])
                    pg_cur.execute('select * from pdq.get_writer_stats_mtd();')
                    writer.writerows(pg_cur)
                wb = openpyxl.Workbook()
                wb.save(mtd_spreadsheet)
                wb = load_workbook(mtd_spreadsheet)
                ws = wb.active
                with open(mtd_file_name) as f:
                    reader = csv.reader(f, delimiter=',')
                    for row in reader:
                        ws.append(row)
                wb.save(mtd_spreadsheet)
        return requests.post(
            "https://api.mailgun.net/v3/notify.cartiva.com/messages",
            auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
            files=[('attachment', ('mtd_writer_stats.xlsx',
                                   open(pdq_dir + 'mtd_writer_stats.xlsx', 'rb').read()))],
            data={"from": "jandrews@cartiva.com <jon@notify.cartiva.com>",
                  "to": ['jandrews@cartiva.com', 'rshroyer@gfhonda.com', 'bfraser@rydellcars.com',
                         'nshirek@rydellcars.com'],
                  "subject": "MTD Writer Stats",
                  "text": "MTD Stats"})


class WriterStatsYesterday(luigi.Task):
    """
    Yesterday's writer stats emailed out Tuesday - Sunday
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield EmailFile()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        """
        """
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select day_name || ' ' || month_name || ' ' || day_of_month::citext || ', ' || the_year::citext
                    from dds.dim_date where the_date = current_date - 1;                 
                """

                pg_cur.execute(sql)
                with open(yesterday_file_name, 'w') as f:
                    writer = csv.writer(f)
                    writer.writerow([pg_cur.fetchone()[0]])
                    writer.writerow([])
                    writer.writerow(["store", "writer #", "writer", "emp #", "lof", "rotate", "air filters",
                                     "cabin filters", "batteries", "bulbs", "wipers"])
                    pg_cur.execute('select * from pdq.get_writer_stats_yesterday();')
                    writer.writerows(pg_cur)
                wb = openpyxl.Workbook()
                wb.save(yesterday_spreadsheet)
                wb = load_workbook(yesterday_spreadsheet)
                ws = wb.active
                with open(yesterday_file_name) as f:
                    reader = csv.reader(f, delimiter=',')
                    for row in reader:
                        ws.append(row)
                wb.save(yesterday_spreadsheet)
        return requests.post(
            "https://api.mailgun.net/v3/notify.cartiva.com/messages",
            auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
            files=[('attachment', ('yesterday_stats.xlsx', open(pdq_dir + 'yesterday_stats.xlsx', 'rb').read()))],
            data={"from": "jandrews@cartiva.com <jon@notify.cartiva.com>",
                  "to": ['jandrews@cartiva.com', 'rshroyer@gfhonda.com', 'nshirek@rydellcars.com'],
                  "subject": "Yesterday PDQ Writer Stats",
                  "text": "Yesterday Stats"})

# if __name__ == '__main__':
#     luigi.run(["--workers=2"], main_task_cls=EmailFile)
