# encoding=utf-8
"""
10/11/21
    since we can no long scrape nissan, we need to build a manual incentives page to facilitate nissan pricing
    this provides a table of current (dealertrack) new nissan inventory for afton to construct what is needed
01/31/2024
    discontinued, don't believe the table is being used anywhere and the entire chrome section
    would have to be refactored to use CVD
02/02/2024
    this was revived because the function hn.get nissan_prices() uses the table hn.nissan_current_new_inventory
    as part of the opentrack updating Dealertrack new vehicle prices

"""

import utilities
import luigi
import datetime
import requests
import ext_arkona

pipeline = 'nissan'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


class UpdateChrome(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtInpmast()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select inpmast_vin
                    from arkona.ext_inpmast a
                    where status = 'I'
                      and type_n_u = 'N'
                      and make = 'Nissan'
                      and not exists (
                        select 1
                        from cvd.nc_vin_descriptions
                        where vin = a.inpmast_vin);
                """
                pg_cur.execute(sql)
                for row in pg_cur.fetchall():
                    vin = row[0]
                    the_date = datetime.datetime.today().strftime('%m/%d/%Y')
                    print(vin)
                    url = 'https://beta.rydellvision.com:8888/cvd/vin-descriptions/' + vin
                    data = requests.get(url)
                    resp = data.text
                    resp = resp.replace("'", "''")
                    with pg_con.cursor() as chr_cur:
                        sql = """
                            insert into cvd.nc_vin_descriptions(vin,response,the_date)
                            values ('{0}', '{1}', '{2}');
                        """.format(vin, resp, the_date)
                        chr_cur.execute(sql)

                        sql = """
                            update cvd.nc_vin_descriptions a
                            set source = b.source,
                                style_count = b.style_count
                            from (   
                                select x.vin, x.response->'result'->>'source' as source,
                                    jsonb_array_length(x.response->'result'->'vehicles') as style_count
                                from cvd.nc_vin_descriptions x
                                where vin = '{0}') b
                            where a.vin = b.vin;
                        """.format(vin)
                        chr_cur.execute(sql)
                        pg_con.commit()


class UpdateInventory(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield UpdateChrome()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select hn.update_nissan_current_new_inventory();"
                pg_cur.execute(sql)


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=UpdateInventory)
