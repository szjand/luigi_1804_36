# encoding=utf-8
"""
05/19/21
    added LastMonthTotalPay
"""
import luigi
import utilities
import datetime
import fact_gl
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email import encoders
import os
import smtplib
from openpyxl import load_workbook
import openpyxl
import csv
from dateutil.relativedelta import relativedelta
import ukg

pipeline = 'bs_est_pay'
db2_server = 'report'
pg_server = '173'


class ClockHours(luigi.Task):
    """
        01/07/22 updated to get clock hours from ukg.clock_hours
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield ukg.XfmClockHours()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select bspp.update_clock_hours();"""
                pg_cur.execute(sql)


class BsSalesDetail(luigi.Task):
    """
    new est/parts/foreman pay plans, written by jeri, implemented 3/8/18
    based on accounting data, much simpler
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        # return fact_gl.FactGl()
        yield fact_gl.FactGl()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select bspp.update_bs_sales_detail()"
                pg_cur.execute(sql)


class LastMonthTotalPay(luigi.Task):
    """
    create a spreadsheet consisting of previous month total pay for estimators on the 4th of each month
    to send to John Gardner
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        file_dir = 'bspp/'
        last_month_file_name = file_dir + 'last_month_total_pay.csv'
        last_month_spreadsheet = file_dir + 'last_month_total_pay.xlsx'
        last_month = datetime.datetime.now() - relativedelta(months=1)
        last_month = format(last_month, '%B %Y')

        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                with open(last_month_file_name, 'w') as f:
                    writer = csv.writer(f)
                    pg_cur.execute("select * from bspp.last_month_total_pay()")
                    writer.writerow(["month", "last name", "first name", "emp #", "job", "total"])
                    writer.writerows(pg_cur)
                wb = openpyxl.Workbook()
                wb.save(last_month_spreadsheet)
                wb = load_workbook(last_month_spreadsheet)
                ws = wb.active
                with open(last_month_file_name) as f:
                    reader = csv.reader(f, delimiter=',')
                    for row in reader:
                        ws.append(row)
                wb.save(last_month_spreadsheet)

        comma_space = ', '
        sender = 'jandrews@cartiva.com'
        # recipients = ['jandrews@cartiva.com', 'test@cartiva.com']
        recipients = ['jandrews@cartiva.com', 'adecker@rydellcars.com']
        outer = MIMEMultipart()
        outer['Subject'] = 'Total wages for ' + last_month
        outer['To'] = comma_space.join(recipients)
        outer['From'] = sender
        attachments = [last_month_spreadsheet]
        for x_file in attachments:
            try:
                with open(x_file, 'rb') as fp:
                    msg = MIMEBase('application', "octet-stream")
                    msg.set_payload(fp.read())
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(x_file))
                outer.attach(msg)
            except Exception:
                raise
        composed = outer.as_string()
        s = smtplib.SMTP('mail.cartiva.com')
        s.sendmail(sender, recipients, composed)
        s.close()


# if __name__ == '__main__':
#     luigi.run(["--workers=4"], main_task_cls=ClockHours)
