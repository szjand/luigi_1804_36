# encoding=utf-8
"""
the updates that need to be done periodically to update tables used for the payroll import files
biweekly: bs estimators, flat rate: gm main shop, honda main shop, body shop
          run every sunday
monthly: sales consultants, fi managers
         run on the first of the month
08/28/2022
  added toyota flat rate
  added a wrapper task to run all of these
  to be run from RunAll, EVERY DAY
  TODO: where appropriate do checks of tech page vs ukg import page, they should always be in sync
"""
import luigi
import utilities
import datetime
import bs_est_pay
import ukg
import team_pay
import fact_repair_order
import ext_arkona

pipeline = 'ukg'
pg_server = '173'


class PopulateUkgImport1(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield bs_est_pay.BsSalesDetail()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):

        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select bspp.populate_ukg_import_1();
                """
                pg_cur.execute(sql)


class UpdateGmMainShopFlatRatePayrollData(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ukg.UpdateEmployees()
        yield ukg.XfmClockHours()
        yield team_pay.DataUpdateNightly()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):

        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select tp.update_gm_main_shop_flat_rate_payroll_data();
                """
                pg_cur.execute(sql)


class UpdateBodyShopFlatRatePayrollData(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ukg.UpdateEmployees()
        yield team_pay.DataUpdateNightly()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):

        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select tp.update_body_shop_flat_rate_payroll_data();
                """
                pg_cur.execute(sql)


class UpdateHnMainShopFlatRatePayrollData(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ukg.UpdateEmployees()
        yield ukg.XfmClockHours()
        yield fact_repair_order.FactRepairOrder()
        yield ext_arkona.ExtSdpxtim()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):

        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select hs.update_hn_main_shop_flat_rate_payroll_data();
                """
                pg_cur.execute(sql)


class UpdateTyMainShopFlatRatePayrollData(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ukg.UpdateEmployees()
        yield ukg.XfmClockHours()
        yield fact_repair_order.FactRepairOrder()
        yield ext_arkona.ExtSdpxtim()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):

        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select ts.update_ty_main_shop_flat_rate_payroll_data();
                """
                pg_cur.execute(sql)


class UkgImports(luigi.WrapperTask):
    """
    a wrapper task that invokes all the classes in this module
    this task is susequently listed as a requirement in run_all
    """
    pipeline = pipeline

    def requires(self):
        yield PopulateUkgImport1()
        yield UpdateGmMainShopFlatRatePayrollData()
        yield UpdateHnMainShopFlatRatePayrollData()
        yield UpdateBodyShopFlatRatePayrollData()
        yield UpdateTyMainShopFlatRatePayrollData()
