# encoding=utf-8
"""
target_date_with_minute: used for generating the output files, include hours & minutes to enable
running a task multiple times a day
but need to be able to verify each "data_today" as a group, thinking make a separated
folder for these targets, at the end, empty the folder, seems to work ok
11/07/20
Removed ExtPypclockinToday and disabled event handling in this module
DataUpdateNightly is called by run_all which implements luigi.Event.START & luigi.Event.SUCCESS,
having that implentation also in this module resulted in double logging for every task invoked by run_all
11/28/21

1/2/22
    removed this from run all, the ukg migration, say no more
    not confident of the accuracy, so in the mean time, scraping advantage scotest.tpdata (and supporting tables) into
    postgresql
02/09/22
    create table tp.test_data, to be updated nightly with call to function tp.test_data_update_nightly()
07/25/22
    finally, sunsetting advantage, this now updates tp.data
    invoked by run_all
"""
import utilities
import luigi
import datetime
import ext_arkona
import fact_repair_order
import ukg

pipeline = 'tp_data_nightly'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
target_directory = '../../luigi_output_files/'
target_date = '{:%Y-%m-%d}'.format(datetime.datetime.now())


class DataUpdateNightly(luigi.Task):
    """
    """

    pipeline = pipeline

    def local_target(self):
        return target_directory + target_date + '_' + self.__class__.__name__ + '.txt'

    def requires(self):
        yield ukg.UpdateEmployees()
        yield ext_arkona.ExtSdpxtim()
        yield fact_repair_order.FactRepairOrder()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select tp.data_update_nightly()"
                pg_cur.execute(sql)
