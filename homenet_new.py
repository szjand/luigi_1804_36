# encoding=utf-8
"""
cronjob: 7 past the hour between 9AN and 9PM mon - sat
ts_with_minute: used for generating the output files, include hours & minutes to enable
running a task multiple times a day
7/12/19:
    need to exclude vehicles with cost < $10K, currently they got to homenet with price coming soon flag set
    to true, but according to yem, the actual numbers go to dealer teamwork
    whatever
    the best place to filter out vehicle based on current cost is in ExtNewData (inpmast)
12/03/20:
    changed the GenerateFile query for December
05/20/21
    for cadillacs, don't send hero card pictures
07/09/21
    removed ctp's from the feed
07/15/22
    added toyota, refactored email to python 3 code
"""
import luigi
import utilities
import csv
import datetime
import ftplib
import opentrack
import requests


pipeline = 'internet_feeds'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class ExtNewData(luigi.Task):
    """
    9/25/19 added best_price to ifd.ext_new_data
    """
    extract_file = '../../extract_files/ext_new_data.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + ts_with_minute + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return opentrack.UpdateNewVehiclePrices()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        """
        Toyota does not have a Best Price configured in Dealertrack
        needed to specify store in joins on inpoptf
        """
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    SELECT trim(inpmast_vin), trim(inpmast_stock_number), 
                      'NEW', year, trim(make), trim(model), trim(model_code), trim(replace(body_Style, '"', '')), 
                      odometer,
                      trim(color) as EXTCOLOR, 
                      trim(trim) as INTCOLOR,
                      e.num_field_value, --invoice
                      list_price, -- msrp
                      b.num_field_value, -- internet price
                      trim(color_code), 
                      g.num_field_value -- best price
                    FROM rydedata.INPMAST a
                    left join rydedata.inpoptf b on a.inpmast_vin = b.vin_number
                      and a.inpmast_company_number = b.company_number                    
                    join rydedata.inpoptd c on b.company_number = c.company_number
                      and b.seq_number = c.seq_number
                      and c.field_descrip = 'Internet Price'
                    left join rydedata.inpoptf e on a.inpmast_vin = e.vin_number
                      and a.inpmast_company_number = e.company_number
                    join rydedata.inpoptd f on e.company_number = f.company_number
                      and e.seq_number = f.seq_number
                      and f.field_descrip = 'Flooring/Payoff'  
                    left join rydedata.inpoptf g on a.inpmast_vin = g.vin_number
                      and a.inpmast_company_number = g.company_number
                    join rydedata.inpoptd h on g.company_number = h.company_number
                      and g.seq_number = h.seq_number
                      and h.field_descrip =  
                        case
                          when a.inpmast_company_number = 'RY1' then 'Best Price'   
                          when a.inpmast_company_number = 'RY8' then 'Internet Price'
                        end                                            
                    where a.status = 'I'
                      and a.type_n_u = 'N'  
                      and a.inpmast_vehicle_cost > 10000      
                      and a.inpmast_company_number in ('RY1','RY8')                        
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate ifd.ext_new_data")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy ifd.ext_new_data from stdin with csv encoding 'latin-1 '""", io)


class GenerateFile(luigi.Task):
    """
    writes a header line
    then appends the data lines
    added field to ifd.homenet_new: price_before_incentives
    9/11/19
    case: when internet_price = 0 then msrp as price_before_incentives
    12/4/2020
        emp inc has changed this month, added join to gmgl.get_vehicle_prices() (dd), if dd.has_emp_inc, then
        price before incentives = dd.testing_price
    01/11/2021
        per yem, cadillac price before incentive should = internet price
    """
    extract_file = '../../extract_files/RydellNew.txt'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + ts_with_minute + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ExtNewData()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as cn:
            with cn.cursor() as cur:
                sql = """ 
                    select replace(string_agg(upper(column_name), '|'), '_','-')
                    from information_schema.columns
                    where table_schema = 'ifd'
                      and table_name = 'homenet_new'                
                """
                cur.execute(sql)
                # headers
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
                cur.execute('select ifd.update_homenet_new_regular_inventory()')
                cn.commit()
                # cur.execute('select ifd.homenet_new_ctp_inventory()')
                # cn.commit()
                cur.execute('truncate ifd.homenet_new')
                # sql = """
                #     insert into ifd.homenet_new
                #     select * from ifd.homenet_new_regular_inventory
                #     union
                #     select * from ifd.homenet_new_ctp_inventory
                # """
                sql = """
                    insert into ifd.homenet_new
                    select * from ifd.homenet_new_regular_inventory       
                """
                cur.execute(sql)
                cur.execute('select * from ifd.homenet_new')
                # data
                with open(self.extract_file, 'a') as f:
                    csv.writer(f, delimiter='|', quotechar='"', quoting=csv.QUOTE_NONE).writerows(cur)


class FtpFile(luigi.Task):
    """
    """
    extract_file = '../../extract_files/RydellNew.txt'
    file_name = 'RydellNew.txt'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + ts_with_minute + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GenerateFile()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # server = 'download.cartiva.com'
        # username = 'download'
        # password = 'th!s!sc@rt!v@ftp'
        # # homenet
        server = 'iol.homenetinc.com'
        username = 'hndatafeed'
        password = 'gx8m6'
        ftp = ftplib.FTP(server)
        ftp.login(username, password)
        ftp.storbinary('STOR '+self.file_name, open(self.extract_file, 'rb'))
        ftp.quit()


class EmailFile(luigi.Task):
    """
    based on python projects/open_ros/email_test.py
    mods:
        a single attachment
        smtp throws: AttributeError: SMTP instance has no attribute '__exit__'
            when invoked in a context manager, maybe a python 2 issue
    """
    extract_file = '../../extract_files/RydellNew.txt'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + ts_with_minute + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield FtpFile()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        return requests.post(
            "https://api.mailgun.net/v3/notify.cartiva.com/messages",
            auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
            files=[('attachment', ('RydellNew.txt', open('../../extract_files/RydellNew.txt', 'rb').read()))],
            data={"from": "jandrews@cartiva.com <jon@notify.cartiva.com>",
                  "to": ['jandrews@cartiva.com', 'mdelohery@rydellcars.com', 'myem@rydellcars.com',
                         'abruggeman@cartiva.com'],
                  "subject": "Homenet New Vehicle Feed",
                  "text": "new car feed"})

# if __name__ == '__main__':
#     luigi.run(["--workers=2"], main_task_cls=EmailFile)
