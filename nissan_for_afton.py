# encoding=utf-8
"""
this gets you into dbs with all statuses and all vehicles on the page
set your own chromedriver_path in function setup_driver
set your own pdf_dir
clicking on the serial generates the detail popup
on that popup, clicking print saves the pdf to your pdf_dir
"""

from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import NoSuchElementException
import os


user = 'XD163692'
password = '%6#@%!!@eDHN'
pdf_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/pdf_files/'


def setup_driver():
    # # Chromedriver path
    chromedriver_path = '/usr/bin/chromedriver'
    options = webdriver.ChromeOptions()
    profile = {"download.default_directory": pdf_dir}
    options.add_experimental_option("prefs", profile)
    options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                         '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
    driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
    return driver


def main():
    driver = setup_driver()
    try:
        driver.get('https://as.na.nissan.biz/SecureAuth71/')
        # SIGNING IN
        driver.find_element_by_class_name('txtUserid').send_keys(user)
        driver.find_element_by_class_name('tbxPassword').send_keys(password)
        driver.find_element_by_class_name('btn-secureauth').click()
        # Click MY LINKS
        WebDriverWait(driver, 10).until(ec.element_to_be_clickable(('id', '1a'))).click()
        # wait until page finishes loading
        WebDriverWait(driver, 10).until(ec.presence_of_element_located(
            ('xpath', '//*[@id="linklisting-containerID"]/div[5]/div/ul/li[1]/a')))
        # load the Nissan DBS page
        driver.get('https://www.nnanet.com/content/nnanet/global/secure/viewlink.link001203.html')
        # load the inventory page
        driver.get('https://dbsapp.nnanet.com/dbs/vehicle/inventory/list?execution=e6s1')
        # select All Locations (statuses)_
        WebDriverWait(driver, 10).until(ec.presence_of_element_located(
            ('id', 'f1:filterValueLocation'))).click()
        WebDriverWait(driver, 10).until(ec.presence_of_element_located(
            ('xpath', '//*[@id="f1:filterValueLocation"]/option[1]'))).click()
        time.sleep(10)
        # show all on page
        driver.find_element_by_xpath('//*[@id="f1:selectPageNumber_newVehicleTable"]').click()
        driver.find_element_by_xpath('//*[@id="f1:selectPageNumber_newVehicleTable"]/option[6]').click()

        row_count = int(driver.find_element_by_xpath('//*[@id="f1:newVehicleTable:newCount"]').text)
        print(row_count)
        # allow all rows to display
        # wait for last element to be visible
        WebDriverWait(driver, 50).until(ec.presence_of_element_located(
            ('id', 'f1:newVehicleTable:%s_row_%s' % (row_count - 1, row_count - 1))))
        # empty the pdf_dir
        for filename in os.listdir(pdf_dir):
            os.unlink(pdf_dir + filename)
        vin_array = []
        for idx in range(0, row_count):
            try:
                # find serial number a tag by id below
                serial_number = driver.find_element_by_id('f1:newVehicleTable:%s:serialNum' % idx)
                # split the onclick action to fetch the entire vin
                full_vin = serial_number.get_attribute('onclick').split('Vin=')[1].split('&')[0]
                # add the vin to the array
                vin_array.append(full_vin)
                print(full_vin)
            except NoSuchElementException:
                # Could not find the serial number above - This means it is an order number.
                # Change the serialNum to orderNum for the idea and it is basically the same otherwise
                order_number = driver.find_element_by_id('f1:newVehicleTable:%s:orderNum' % idx)
                # There will not be a vin obviously BUT Nissan provides a "temp" vin I am calling it so you
                # can still do a "View Vehicle Details" on it. You may get the order number from the details page
                # or we can get it here
                temp_vin = order_number.get_attribute('onclick').split('Vin=')[1].split('&')[0]
                # add the temp_vin to the array
                vin_array.append(temp_vin)
                print(temp_vin)

        for vin in vin_array:
            driver.get('https://dbsapp.nnanet.com/dbs/vehicle/inventory/list/print?selectedVin=%s&detailFlag=1' % vin)
            WebDriverWait(driver, 20).until(ec.element_to_be_clickable(('xpath', '//*[@id="f2:comPrintIcon"]'))).click()

    finally:
        driver.quit()


if __name__ == '__main__':
    main()
