# encoding=utf-8
"""
scrape honda interactive network for vehicle status, invoices and incentives
5/2 status parse, invoice parse,
    incentive download
    incentive parse: fails when publish date has changed
5/8/20
    realized, the output file consists of the date and the class name (the local_target)
    tried to run honda incentives today, nothing ran, because the class name for nissan incentives is
    IncentivesDownload, the same as what i have named the classes here
    for this file, i will include the pipeline in the definition of local_target
    worked for IncetivesDownload, do it for all classes in this file
"""

import utilities
import luigi
import camelot
import datetime
import os
from selenium import webdriver
import time
import csv
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from PyPDF2 import utils
import shutil
import requests

pipeline = 'honda'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
dealer_number = '207818'

user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None

local_or_production = 'production'
# local_or_production = 'local'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class VehicleStatusDownload(luigi.Task):
    """
    download vehicle status spreadsheet as VehicleStatus.csv
    """
    pipeline = pipeline

    if local_or_production == 'local':
        vehicle_status_dir = '/home/jon/projects/luigi_1804_36/honda/vehicle_status/'
        clean_vehicle_status_dir = '/home/jon/projects/luigi_1804_36/honda/clean_vehicle_status/'
    elif local_or_production == 'production':
        vehicle_status_dir = '/home/rydell/projects/luigi_1804_36/honda/vehicle_status/'
        clean_vehicle_status_dir = '/home/rydell/projects/luigi_1804_36/honda/clean_vehicle_status/'

    file_name = 'VehicleStatus.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.vehicle_status_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    def run(self):
        for filename in os.listdir(self.vehicle_status_dir):
            os.unlink(self.vehicle_status_dir + filename)
        driver = self.setup_driver()
        try:
            driver.get('https://www.in.honda.com/RRAAApps/login/asp/rraalog.asp')
            credentials = self.get_secret('Cartiva', 'Honda')
            resp_dict = credentials
            driver.find_element_by_xpath('//*[@id="txtDlrNo"]').send_keys(dealer_number)
            username_box = driver.find_element_by_xpath('//*[@id="txtLogonID"]')
            password_box = driver.find_element_by_xpath('//*[@id="txtPassword"]')
            login_btn = driver.find_element_by_xpath('//*[@id="btnLogon"]')
            username_box.send_keys(resp_dict.get('username'))
            password_box.send_keys(resp_dict.get('secret'))
            login_btn.click()
            # EXECUTIVE MANAGEMENT
            WebDriverWait(driver, 1).until(
                ec.presence_of_element_located
                (('xpath', '//*[@id="tblTabs"]/tbody/tr/td[13]'))).click()
            # Vehicle Status
            WebDriverWait(driver, 1).until(
                ec.presence_of_element_located(('xpath', '//*[@id="RDAAAH060000"]/table'))).click()
            # everything on the Vehicle Status page is in an iframe name Content
            WebDriverWait(driver, 30).until(
                ec.frame_to_be_available_and_switch_to_it((By.XPATH, "//iframe[@id='Content']")))
            # Status: all check boxes
            WebDriverWait(driver, 30).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="ctl00_ctl00_M_content_isInTransitCheckBox"]'))).click()
            driver.find_element_by_xpath('//*[@id="ctl00_ctl00_M_content_isOnHandCheckBox"]').click()
            driver.find_element_by_xpath('//*[@id="ctl00_ctl00_M_content_isDemoCheckBox"]').click()
            # submit button
            driver.find_element_by_xpath('// *[ @ id = "ctl00_ctl00_M_content_submitButton"]').click()
            # Export to Excel link
            WebDriverWait(driver, 10).until(
                ec.element_to_be_clickable
                (('xpath',
                  '//*[@id="ctl00_ctl00_M_content_topPanel"]/table/tbody/tr[1]/td/table/tbody/tr/td[3]/a[4]'))).click()

            time.sleep(10)
        finally:
            driver.quit()


class VehicleStatusParse(luigi.Task):
    """
    clean csv of non data rows
    insert into hn.ext_hin_vehicle_statuses
    """
    pipeline = pipeline
    file_name = 'VehicleStatus.csv'

    if local_or_production == 'local':
        vehicle_status_dir = '/home/jon/projects/luigi_1804_36/honda/vehicle_status/'
        clean_vehicle_status_dir = '/home/jon/projects/luigi_1804_36/honda/clean_vehicle_status/'
    elif local_or_production == 'production':
        vehicle_status_dir = '/home/rydell/projects/luigi_1804_36/honda/vehicle_status/'
        clean_vehicle_status_dir = '/home/rydell/projects/luigi_1804_36/honda/clean_vehicle_status/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        yield VehicleStatusDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        for filename in os.listdir(self.clean_vehicle_status_dir):
            os.unlink(self.clean_vehicle_status_dir + filename)
        with open(self.vehicle_status_dir + self.file_name, 'r') as infile, open(
                self.clean_vehicle_status_dir + self.file_name, 'w') as outfile:
            reader = csv.reader(infile)
            # need to quote all fields to persist numbers with leading zeros
            writer = csv.writer(outfile, quoting=csv.QUOTE_ALL)
            for row in reader:
                try:
                    # remove non data
                    if len(row) > 20 and ','.join(row)[:2] != 'St' and ','.join(row)[:6] != 'Search':
                        writer.writerow(item for item in row)
                except IndexError:  # skip zero length rows
                    continue
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate hn.ext_hin_vehicle_statuses")
                with open(self.clean_vehicle_status_dir + self.file_name, 'r') as io:
                    pg_cur.copy_expert("""copy hn.ext_hin_vehicle_statuses from stdin with csv encoding 'latin-1 '""",
                                       io)
                pg_cur.execute('select hn.update_hin_vehicle_statuses()')


class InvoiceDownload(luigi.Task):
    """
    for vins in hn.hin_vehicle_statuses, download invoices and rename to vin.pdf
    """
    pipeline = pipeline

    if local_or_production == 'local':
        invoice_dir = '/home/jon/projects/luigi_1804_36/honda/invoice_download/'
    elif local_or_production == 'production':
        invoice_dir = '/home/rydell/projects/luigi_1804_36/honda/invoice_download/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        yield VehicleStatusParse()

    def output(self):
        # define output filename and pathhttps://www.in.honda.com/RRAAApps/login/asp/rraalog.asp
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        options.add_experimental_option("prefs", {
            "download.default_directory": self.invoice_dir,
            "download.prompt_for_download": False,
            "plugins.always_open_pdf_externally": True})
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    def run(self):
        for filename in os.listdir(self.invoice_dir):
            os.unlink(self.invoice_dir + filename)
        driver = self.setup_driver()
        try:
            old_file_name = self.invoice_dir + 'RDXA_AIIM_VehicleInvoice.pdf'
            driver.get('https://www.in.honda.com/RRAAApps/login/asp/rraalog.asp')
            credentials = self.get_secret('Cartiva', 'Honda')
            resp_dict = credentials
            driver.find_element_by_xpath('//*[@id="txtDlrNo"]').send_keys(dealer_number)
            username_box = driver.find_element_by_xpath('//*[@id="txtLogonID"]')
            password_box = driver.find_element_by_xpath('//*[@id="txtPassword"]')
            login_btn = driver.find_element_by_xpath('//*[@id="btnLogon"]')
            username_box.send_keys(resp_dict.get('username'))
            password_box.send_keys(resp_dict.get('secret'))
            login_btn.click()
            # SALES
            driver.find_element_by_xpath('//*[@id="tblTabs"]/tbody/tr/td[3]').click()
            # Vehicle Invoices
            # WebDriverWait(driver, 1).until(
            #     ec.presence_of_element_located(('xpath', '//*[@id="RDAASH130000"]/table'))).click()
            WebDriverWait(driver, 1).until(
                ec.presence_of_element_located(('xpath', '//*[@id="RDAASH130000"]/table/tbody/tr/td[1]'))).click()
            # this page is in an iframe named Content
            WebDriverWait(driver, 20).until(
                ec.frame_to_be_available_and_switch_to_it((By.XPATH, "//iframe[@id='Content']")))
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    sql = """
                            select vin
                            from hn.hin_vehicle_statuses a
                            where status in ('onhand','shipped')
                              and not exists (
                                select 1
                                from hn.hin_vehicle_invoices
                                where vin = a.vin)
                        """
                    pg_cur.execute(sql)
                    for row in pg_cur.fetchall():
                        vin = row[0]
                        new_file_name = self.invoice_dir + vin + '.pdf'
                        # enter vin
                        # vin_text_box = driver.find_element_by_xpath('//*[@id="aspnetForm"]/table/tbody/'
                        #                                             'tr[2]/td/div[1]/table/tbody/tr[2]/td[2]/input')
                        vin_text_box = WebDriverWait(driver, 20).until(ec.presence_of_element_located(
                            (By.XPATH, '//*[@id="aspnetForm"]/table/tbody/'
                                       'tr[2]/td/div[1]/table/tbody/tr[2]/td[2]/input')))
                        vin_text_box.clear()
                        vin_text_box.send_keys(vin)
                        # click on invoice number link instead
                        # occasionally the invoice number is not a link
                        try:
                            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                                (By.XPATH, '//*[@id="Table2"]/tbody/tr/td[3]/a'))).click()
                        except TimeoutException:
                            continue
                        # wait for invoice window to open
                        try:
                            WebDriverWait(driver, 20).until(ec.number_of_windows_to_be(2))
                        except TimeoutException:
                            try:  # if the invoice link didn't work, try the print checkbox and print button
                                driver.find_element_by_xpath('//*[@id="Table2"]/tbody/tr/td[1]/input').click()
                                driver.find_element_by_xpath(
                                    '//*[@id="aspnetForm"]/table/tbody/tr[2]/td/div[4]/input').click()
                            except TimeoutException:
                                continue
                        time.sleep(5)
                        driver.switch_to.window(driver.window_handles[1])
                        driver.close()
                        driver.switch_to.window(driver.window_handles[0])
                        # rename the downloaded file from RDXA_AIIM_VehicleInvoice.pdf to vin.pdf
                        os.rename(old_file_name, new_file_name)
                        # refresh focus to iframe
                        WebDriverWait(driver, 20).until(
                            ec.frame_to_be_available_and_switch_to_it((By.XPATH, "//iframe[@id='Content']")))
        finally:
            driver.quit()


class InvoiceParse(luigi.Task):
    """
    parse invoice pdfs, populate hn.ext_hin_vehicle_invoices values
    extract just the portions of the pdf that include the data we need
    """
    pipeline = pipeline

    if local_or_production == 'local':
        invoice_dir = '/home/jon/projects/luigi_1804_36/honda/invoice_download/'
        csv_dir = '/home/jon/projects/luigi_1804_36/honda/invoice_csv/'
        bad_invoices_dir = '/home/jon/projects/luigi_1804_36/honda/bad_invoices/'
    elif local_or_production == 'production':
        invoice_dir = '/home/rydell/projects/luigi_1804_36/honda/invoice_download/'
        csv_dir = '/home/rydell/projects/luigi_1804_36/honda/invoice_csv/'
        bad_invoices_dir = '/home/rydell/projects/luigi_1804_36/honda/bad_invoices/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        yield InvoiceDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate hn.ext_hin_vehicle_invoices")
                pg_con.commit()
            for file_name in os.listdir(self.invoice_dir):
                vin = os.path.splitext(file_name)[0]
                for csv_file in os.listdir(self.csv_dir):
                    os.unlink(self.csv_dir + csv_file)
                # define a specific "table" (range) for each data point
                # when i added the 2 additional "tables" to get the base invoice and destination charge,
                # the order in which the tables were read became unpredictable
                try:
                    tables = camelot.read_pdf(
                        self.invoice_dir + file_name, flavor='stream', table_areas=['440,702,479,688'], strip_text='\n')
                except utils.PdfReadError:
                    source = self.invoice_dir + file_name
                    destination = self.bad_invoices_dir + file_name
                    shutil.move(source, destination)
                    continue
                tables[0].to_csv(self.csv_dir + vin + '-0.csv')  # invoice date
                tables = camelot.read_pdf(
                    self.invoice_dir + file_name, flavor='stream', table_areas=['173,675,215,662'], strip_text='\n')
                tables[0].to_csv(self.csv_dir + vin + '-1.csv')  # invoice_number
                tables = camelot.read_pdf(
                    self.invoice_dir + file_name, flavor='stream', table_areas=['64,240,275,225'], strip_text='\n')
                tables[0].to_csv(self.csv_dir + vin + '-2.csv')  # msrp
                tables = camelot.read_pdf(
                    self.invoice_dir + file_name, flavor='stream', table_areas=['485,213,547,197'], strip_text='\n')
                tables[0].to_csv(self.csv_dir + vin + '-3.csv')  # total_invoice
                tables = camelot.read_pdf(
                    self.invoice_dir + file_name, flavor='stream', table_areas=['500,465,550,450'], strip_text='\n')
                tables[0].to_csv(self.csv_dir + vin + '-4.csv')  # base_invoice
                tables = camelot.read_pdf(
                    self.invoice_dir + file_name, flavor='stream', table_areas=['513,439,550,425'], strip_text='\n')
                tables[0].to_csv(self.csv_dir + vin + '-5.csv')  # destination_chartge
                with open(self.csv_dir + vin + '-0.csv') as i_d:
                    id_reader = csv.reader(i_d)
                    for row in id_reader:
                        invoice_date = row[0]
                with open(self.csv_dir + vin + '-1.csv') as i_n:
                    in_reader = csv.reader(i_n)
                    for row in in_reader:
                        invoice_number = row[0]
                with open(self.csv_dir + vin + '-2.csv') as m:
                    m_reader = csv.reader(m)
                    for row in m_reader:
                        msrp = row[0]
                with open(self.csv_dir + vin + '-3.csv') as ti:
                    ti_reader = csv.reader(ti)
                    for row in ti_reader:
                        total_invoice = row[0]
                with open(self.csv_dir + vin + '-4.csv') as bi:
                    bi_reader = csv.reader(bi)
                    for row in bi_reader:
                        base_invoice = row[0]
                with open(self.csv_dir + vin + '-5.csv') as dc:
                    dc_reader = csv.reader(dc)
                    for row in dc_reader:
                        destination_charge = row[0]
                with pg_con.cursor() as pg_cur:
                    sql = """
                        insert into hn.ext_hin_vehicle_invoices values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')
                    """.format(vin, invoice_date, invoice_number, msrp, total_invoice, base_invoice, destination_charge)
                    pg_cur.execute(sql)
                    pg_con.commit()
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select hn.hin_vehicle_invoices()")


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=InvoiceParse)
