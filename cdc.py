# encoding=utf-8
"""
!!!!!!!!!!!!!!!
cdc table is on a separate sever, 172.17.196.106 PG_CDC running PG 15
!!!!!!!!!!!!!!!
"""
import luigi
import utilities
import csv
import datetime
import fact_repair_order

pipeline = 'cdc'
db2_server = 'report'


class ExtPdpmast(luigi.Task):
    """
    runs nightly in luigi as part of generating the pdmast cdc table
    """
    extract_file = '../../extract_files/cdc_pdpmast.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    SELECT TRIM(COMPANY_NUMBER),TRIM(MANUFACTURER),TRIM(PART_NUMBER),TRIM(PART_DESCRIPTION),
                        TRIM(STOCKING_GROUP),TRIM(STATUS),TRIM(OBSOLETE),TRIM(RETURN_CODE),TRIM(ARRIVAL_CODE),
                        TRIM(STOCK_PROMO_CODE),TRIM(SPEC_ORDER_CODE),TRIM(PRICE_UPDATE),TRIM(KIT_PART),
                        TRIM(COMPONENT_PART),TRIM(ASSOC_WITH_PART),TRIM(ALTERNATE_PART),TRIM(IS_CORE_PART),
                        TRIM(MERCH_CODE),TRIM(PRICE_SYMBOL),TRIM(PROD_CODE_CLASS),TRIM(FLEET_ALLOWANCE),
                        TRIM(GROUP_CODE),TRIM(BIN_LOCATION),TRIM(SHELF_LOCATIION),QTY_ON_HAND,QTY_RESERVED,
                        QTY_ON_ORDER,QTY_ON_BACK_ORDER,QTY_ON_SPEC_ORDER,PLACE_ON_ORDER,PLACE_ON_SPEC_ORD,
                        PACK_QTY,MIN_SALES_QTY,COST,LIST_PRICE,TRADE_PRICE,WHLSE_COMP,WHLSE_COMP_FLEET,
                        FLAT_PRICE,MIN_MAX_PACK_ADJ,DYNAM_DAYS_SUPPLY,MINIMUM_ON_HAND,MAXIMUM_ON_HAND,
                        LOW_MODEL_YEAR,HIGH_MODEL_YEAR,DATE_IN_INVENTORY,DATE_PHASED_OUT,DATE_LAST_SOLD,
                        INVENTORY_TURNS,BEST_STOCK_LEVEL,STOCK_STATUS,RECENT_DEMAND,PRIOR_DEMAND,
                        RECENT_WORK_DAYS,PRIOR_WORK_DAYS,WEIGHTED_DLY_AVG,SALES_DEMAND,LOST_SALES_DEMAND,
                        SPEC_ORDER_DEMAND,NON_STOCK_DEMAND,RETURN_DEMAND,INVENTORY_VALUE,TOT_QTY_SOLD,
                        TOT_COST_SALES,STOCK_PURCHASES,TRIM(DISPLAY_PART_NO_),TRIM(SORT_PART_NUMBER),
                        TRIM(OLD_PART_NUMBER),TRIM(NEW_PART_NUMBER),TRIM(CORE_PART_NUMBER),TRIM(REMARKS),
                        TRIM(DISPLAY_REMARKS),BULK_ITEM_ACTUAL_WEIGHT,TRIM(SECONDARY_BIN_LOCATION2),
                        TRIM(CLASS_CODE),PART_DEALER_TO_DEALER_PRICE,PART_SUBSIDIARY_PRICE,
                        TRIM(PART_DEALER_TO_DEALER_CODE),EMERGENCY_REPAIR_PACKAGE_QUA,
                        TRIM(PART_LSG_CODE),TRIM(BRAND_CODE),TRIM(PART_COMMOND_CODE),
                        TRIM(PART_CROSS_SHIP_CODE),TRIM(PART_DISCOUNT_CODE),TRIM(PART_DIRECT_SHIP_CODE),
                        TRIM(PART_HAZARD_CODE),TRIM(PART_INTERCHANGE_SUBSTITUTIO),
                        TRIM(PART_LARGE_QUANTITY_CODE),PART_MAXIMUM_ORDER_QUANTITY,
                        PART_MINIMUM_ORDER_QUANTITY,TRIM(PART_OBSOLETE_STATUS),
                        TRIM(PROGRAM_PART_ALLOWANCE_CODE),PART_PRICING_DISCOUNT_PERCEN,
                        TRIM(PROGRAM_PART_RETURNABLE_CODE),TRIM(PART_SIZE_CODE),TRIM(PART_TYPE),
                        TRIM(PART_VOLUME_DISCOUNT_FLAG),TRIM(SECONDARY_SHELF2),TRIM(WEIGHT_CODE),
                        UNIT_OF_MEASURE,LAST_RECEIPT_DATE,FIRST_RECEIPT_DATE,TRIM(PART_ZONE),
                        SPIFF_BONUS_AMOUNT,TRIM(ALIAS),HAZ_MAT_DISPOSAL_FEE,TRIM(MANUAL_ORDER_PART),
                        LAST_CHANGE_TIMESTAMP,TRIM(LAST_UPDATE_USER),PART_WEIGHT,SHIPPING_HEIGHT,
                        SHIPPING_LENGTH,SHIPPING_DEPTH,TRIM(TURNOVER_CODE)
                    from rydedata.pdpmast
                    where company_number in ('RY1','RY2','RY8')
                      and left(part_number, 1) <> ' '
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg('cdc') as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pdpmast")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pdpmast from stdin with csv encoding 'latin-1 '""", io)


class PdpmastSnapshot(luigi.Task):
    """
    creates a table named for the current date, eg arkona_cdc.pdpmast_20200207, and populates it with
    the current days scrape of pdpmast
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ExtPdpmast()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg('cdc') as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = " select arkona_cdc.create_pdpmast_snapshot() "
                pg_cur.execute(sql)


class ExtFactRepairOrder(luigi.Task):
    """
    scrape the fact_repair_order table from the production server to the cdc server
    """
    extract_file = '../../extract_files/cdc_fact_repair_order.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return fact_repair_order.FactRepairOrder()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg('173') as pg_173:
            with pg_173.cursor() as cur:
                sql = """
                    SELECT * 
                    from dds.fact_repair_order
                    where open_date > '12/31/2023'
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg('cdc') as pg_cdc:
            with pg_cdc.cursor() as pg_cur:
                pg_cur.execute("truncate dds.ext_fact_repair_order")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy dds.ext_fact_repair_order from stdin with csv encoding 'latin-1 '""", io)


class FactRepairOrderSnapshot(luigi.Task):
    """
    creates a table named for the current date, eg cdc.fact_repair_order_20240115, and populates it with
    the previous days scrape of dds.fact_repair_order
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ExtFactRepairOrder()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg('cdc') as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select cdc.create_fact_repair_order_snapshot() "
                pg_cur.execute(sql)


class CdcWrapper(luigi.WrapperTask):
    """
    a wrapper task that invokes all the classes in this module
    this task is susequently listed as a requirement in run_all
    that way i don't need to construct artificial dependencies just to ensure everything gets run
    """
    pipeline = pipeline

    def requires(self):
        yield PdpmastSnapshot()
        yield FactRepairOrderSnapshot()


# if __name__ == '__main__':
#     luigi.run(["--workers=1"], main_task_cls=FactRepairOrderSnapshot)
